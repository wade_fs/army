#!/bin/bash
# Step 1: 將兩個檔要一起壓縮成 auto_run.zip,
#         本 auto_run.zip 由平板端負責 呼叫本程式製成的 自解壓縮 script file 來解壓縮
#   1) 要複製的檔 ATTACHED_FILE
#   2) 解壓縮後要執行的檔，用來將檔案複製到目標去

# FILE1 : 由參數指定要複製的檔，這裡只設定檔名
[ "x$1" = "x" ] && echo "Usage: %0 ATTACHED" && exit 1 || ATTACHED=$1
ATTACHED_FILE=`basename $ATTACHED`
cp $ATTACHED $ATTACHED_FILE 2>/dev/null
[ ! -f "$ATTACHED_FILE" ] && echo "$ATTACHED is not exist or cannot copy to $ATTACHED" && exit 2
# FILE2 : 準備解壓後要執行的碼，叫 runner.sh
cat  <<RUNNER > runner.sh
[ ! -f /storage/sdcard0/$ATTACHED_FILE ] && mv $ATTACHED_FILE /storage/sdcard0/
[ ! -f /storage/sdcard0/$ATTACHED_FILE ] && am start -n com.example.hydra.mt6582_notifier/.MainActivity --es "STRINGMSG" "檔案複製失敗" || am start -n com.example.hydra.mt6582_notifier/.MainActivity --es "STRINGMSG" "檔案複製完成，請檢查"
RUNNER

# Step 2: 壓縮檔案，檔名就叫 auto_run.zip
AUTO_RUN=auto_run
zip -P 80539283 ${AUTO_RUN}.zip $ATTACHED_FILE runner.sh
SHELL_NAME=${AUTO_RUN}.sh-copy-file-$ATTACHED_FILE

# Append uuencoded data.
echo -e "#!/system/bin/sh\n" > ${SHELL_NAME}

cat <<HERE >> ${SHELL_NAME}
function untar_payload()
{
        match=\$(busybox grep -n '^PAYLOAD:\$' \$0 | busybox cut -d ':' -f 1)
        payload_start=\$((match + 1))
        busybox tail -n +\$payload_start \$0 | busybox uudecode
}

untar_payload

exit 0
HERE

echo "PAYLOAD:" >> ${SHELL_NAME}

cat ${AUTO_RUN}.zip | uuencode - >>${SHELL_NAME}
rm -f ${AUTO_RUN}.zip $ATTACHED_FILE runner.sh
