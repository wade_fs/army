#!/bin/bash

catInstallSh() {
cat <<HERE
function untar_payload()
{
        match=\$(busybox grep -n '^PAYLOAD:\$' \$0 | busybox cut -d ':' -f 1)
        payload_start=\$((match + 1))
        if [[ \$binary -ne 0 ]]; then
           busybox tail -n +\$payload_start \$0
        fi
        if [[ \$uuencode -ne 0 ]]; then
                busybox tail -n +\$payload_start \$0 | busybox uudecode
        fi
}

untar_payload

exit 0
HERE
}

uuencode=1
binary=0
FILENAME=auto_run.zip

if [[ "$1" == '--binary' ]]; then
        binary=1
        uuencode=0
        shift
fi
if [[ "$1" == '--uuencode' ]]; then
        binary=0
        uuencode=1
        shift
fi
if [ "x$1" = "x-v" ]; then
  shift
  versionName=$1
  shift
else
  versionName=`grep 'versionName "' ../../app/build.gradle  | sed -e 's/.*e "\([^"]*\)"/\1/'`
fi
if [ ! "x$versionName" = "x" -a -e ../../app/release/com.imobile.artillerybattalion-${versionName}.apk ]; then
  ls -l ../../app/release/com.imobile.artillerybattalion-${versionName}.apk
  cp -f ../../app/release/com.imobile.artillerybattalion-${versionName}.apk artillerybattalion.apk
fi
A1="-d"
if [ "x${A1}" = "x-d" ]; then
  FILES="artillerybattalion.apk runner.sh"
  shift
cat > runner.sh <<RUNNER
#!/bin/bash

APKNAME=artillerybattalion
SYSTEMPATH=/system/local_script/
SDPATH=/storage/sdcard0/

UPDATE_VENDER(){
#        rm -rf /data/local/tmp/com.imobile.artillerybattalion-build-id.txt
#        rm -rf /data/data/com.imobile.artillerybattalion
#        rm -rf /data/app/com.imobile.artillerybattalion-1
#        rm -rf /data/dalvik-cache/profiles/com.imobile.artillerybattalion
#        rm -rf /data/dalvik-cache/arm/data@app@com.imobile.artillerybattalion-1@base.apk@classes.dex
#        rm -rf /data/app-lib/artillerybattalion
#        rm -rf/data/dalvik-cache/arm/system@vendor@operator@app@artillerybattalion@artillerybattalion.apk@classes.dex
#        rm -rf /system/vendor/operator/app/artillerybattalion
        pm install -r \$SDPATH\$APKNAME.apk
        sleep 5
        rm \$SDPATH\$APKNAME.apk
        sleep 2
        am start -n com.example.hydra.mt6582_notifier/.MainActivity --es "STRINGMSG" "更新完成，請重新開機"
}

UPDATE_VENDER
RUNNER
else
  FILES=
fi

if [  "x$1" = "x" -a "x$FILES" = "x" ]; then
        echo "Usage: $0 [--binary | --uuencode] PAYLOAD_FILE PAYLOAD_FILE ..."
        exit 1
fi

zip -P 80539283 auto_run.zip $FILES $@
SHELL_NAME=auto_run.sh-artillerybattalion-${versionName}
if [[ $binary -ne 0 ]]; then
        # Append binary data.
        echo -e "#!/bin/bash\nbinary=1\nuuencode=0\nfilename=$FILENAME\n" > ${SHELL_NAME}
        catInstallSh >> ${SHELL_NAME}
        echo "PAYLOAD:" >> ${SHELL_NAME}

        cat $FILENAME >>${SHELL_NAME}
fi
if [[ $uuencode -ne 0 ]]; then
        # Append uuencoded data.
        echo -e "#!/bin/bash\nbinary=0\nuuencode=1\nfilename=$FILENAME\n" > ${SHELL_NAME}
        catInstallSh >> ${SHELL_NAME}
        echo "PAYLOAD:" >> ${SHELL_NAME}

        cat $FILENAME | uuencode - >>${SHELL_NAME}
fi
rm -f $FILENAME $FILES
