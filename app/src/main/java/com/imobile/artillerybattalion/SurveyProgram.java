package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

// import com.imobile.libs.Tools;

import com.imobile.libs.Tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class SurveyProgram extends Activity
{
    private final String TAG="MyLog";
    private SurveyProgram mainActivity;
    private Button lastBtn;
    private Drawable buttonBG, spinnerBG;
    private int setSource=0;
    private int ids[];
    private List<EditText> ets;

    ScrollView spSV;
    Spinner spM, spTP, spO, spT, spG;
    Button btScp, btP, btAdd;
    LinearLayout spLL;
    TextView tvPA;
    EditText etPA;
    Date date;
    int acc=0;
    double diff=0.0, closedPathLen=0.0;

    int row;

    private class LL {
        int start, end;
        EditText A, D, V;
        LinearLayout linearLayout;
        LL(int s, int e, EditText a, EditText d, EditText v, LinearLayout l) {
            start = s; end = e; linearLayout = l;
            A = a;
            D = d;
            V = v;
        }
        String A() { return A.getText().toString().trim(); }
        String D() { return D.getText().toString().trim(); }
        String V() { return V.getText().toString().trim(); }
    }
    List<LL> ll;
    // 所謂測試點，主要是用在向量的兩端
    private class EndPoint {
        public int id; // id 端點測站的 id, 請見 Station.id 的說明
        private int idA, idD, idV;
        EndPoint(int tId, EditText tA, EditText tD, EditText tV) {
            id = tId;
            idA = (tA == null)?-1:tA.getId();
            idD = (tD == null)?-1:tD.getId();
            idV = (tV == null)?-1:tV.getId();
        }
        String A() { return idA>0?((EditText)findViewById(idA)).getText().toString().trim()+" ":" "; }
        String D() { return idD>0?((EditText)findViewById(idD)).getText().toString().trim()+" ":" "; }
        String V() { return idV>0?((EditText)findViewById(idV)).getText().toString().trim()+" ":" "; }
        void setADV(EditText A, EditText D, EditText V) {
            idA = A.getId();
            idD = D.getId();
            idV = V.getId();
        }
    }
    // 每個測站，都是一個測試點,
    // 而每個測站，都會有目標與來源兩種向量，以目標為主體，來源只是指示『多重來源』的測站用的
    private class Station {
        public int id; // 這邊的 id 存的是上方下拉式選單的位置，當然五個下拉式選單分別是 1xxx, 2xxx, 3xxx, 4xxx, 5xxx;
        public double x, y, h;  // 只有 SCP 是從 GUI 輸入的，其餘的測站，這三個值是需要計算的
        private double a, p; // a 是方位角，p 則是從 P 點來的角度
        public String name;

        Station (int sId) {
            this.id = sId;
            this.p = -10000.0;
            this.a = -10000.0;
            x = y = h = -10000.0;
            targets = new ArrayList<>();
            sources = new ArrayList<>();
            name = getSpinnerItemTextByStationId(id);
            if (id > 3200 && id < 4000) {
                name = name + "方位基準點";
            } else if (id > 5200 && id < 5400) {
                name = name;
            } else if (id > 5400) {
                name = name + "方向基線";
            }
        }
        private List<EndPoint> targets; // 以本站為來源
        private List<Integer> sources; // 以本站為目標, 來源只存 station id
        private int getTargetPos(int id) {
            if (targets != null)
            for (int i=0; i<targets.size(); i++) {
                if (targets.get(i).id == id) return i;
            }
            return -1;
        }
        private EndPoint getTarget(int id) {
            if (targets != null)
                for (int i=0; i<targets.size(); i++) {
                    if (targets.get(i).id == id) return targets.get(i);
                }
            return null;
        }
        private void setTarget(int tId, EditText tA, EditText tD, EditText tV) {
            EndPoint endPoint = getTarget(tId);
            if (endPoint != null) {
                endPoint.setADV(tA, tD, tV);
            } else {
                endPoint = new EndPoint(tId, tA, tD, tV);
                targets.add(endPoint);
            }
            ets.add(tA);
            if (tD != null) {
                ets.add(tD);
                tD.setFocusableInTouchMode(true);
            }
            if (tV != null) {
                ets.add(tV);
                tV.setFocusableInTouchMode(true);
            }
            lastEditText = tA;
            Station e = getStation(tId);
            if (e != null) {
                e.setSource(this.id);
            }
        }
        private void rmTarget(int tId) {
            int pos = getTargetPos(tId);
            if (pos >= 0) targets.remove(pos);
            Station s = getStation(tId);
            if (s != null) {
                s.rmSource(this.id);
                if (s.sources.size() <= 0) {
                    rmStation(tId);
                }
            }
        }
        void rmSource(int id) {
            int pos = getSourcePos(id);
            if (pos >= 0) sources.remove(pos);
        }
        private int getSourcePos(int id) {
            for (int i=0; i<sources.size(); i++) {
                if (sources.get(i) == id) return i;
            }
            return -1;
        }
        private void setSource(int tId) {
            if (getSourcePos(tId) < 0) {
                sources.add(tId);
            }
        }
        private boolean isId(int id) { return this.id == id; }
    }
    List<Station> stations;
    Spinner lastSpinner;
    LinearLayout lastLL;
    int lastSpInStationId;
    private Spinner setSpinner(int spId, int stArray) {
        Spinner spinner = (Spinner)findViewById(spId);
        ArrayAdapter<String> aa = new ArrayAdapter<>(this, R.layout.spinner_item, getResources().getStringArray(stArray));
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);
        return spinner;
    }
    void rmStation(int sId) {
        for (Station s : stations) {
            if (s.isId(sId)) { stations.remove(s); return; }
        }
    }
    public int getStationPos(int sId) {
        for (int i=0; i<stations.size(); i++) {
            if (stations.get(i).isId(sId)) return i;
        }
        return -1;
    }
    public Station getStation(int sId) {
        for (Station s : stations) {
            if (s.isId(sId)) { return s; }
        }
        return null;
    }
    EditText lastEditText = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.survey_program);
        mainActivity = this;

        spSV  = (ScrollView)findViewById(R.id.spSV);
        spLL  = (LinearLayout) findViewById(R.id.spLL);
        btScp = (Button)findViewById(R.id.SCP);
        btP   = (Button)findViewById(R.id.btP);
        btAdd = (Button)findViewById(R.id.btSPAdd);
        tvPA  = (TextView)findViewById(R.id.tvPA);
        etPA  = (EditText)findViewById(R.id.etPA);

        spM  = setSpinner(R.id.spM, R.array.stM);
        spinnerBG = spM.getBackground();
        buttonBG = btScp.getBackground();
        spM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2) {
                    Warn("此項目不提供選取，請點選其他項目");
                    spM.setSelection(0);
                    return;
                } else if (position == 0) return;
                if (3 <= position && position <= 12)
                    selectedSpinnerByStationId(1000+position, spM, 1);
                else
                    selectedSpinnerByStationId(1000+position, spM, 0);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) { parent.setSelection(0);}
        });
        spTP = setSpinner(R.id.spTP, R.array.stTP);
        spTP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                selectedSpinnerByStationId(2000+position, spTP, 0);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) { parent.setSelection(0); }
        });
        spO  = setSpinner(R.id.spO, R.array.stO); // 3xxx
        spO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                selectedSpinnerByStationId(3000+position, spO, 1);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) { parent.setSelection(0); }
        });
        spT  = setSpinner(R.id.spT, R.array.stT);
        spT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                selectedSpinnerByStationId(4000+position, spT, 0);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) { parent.setSelection(0); }
        });
        spG  = setSpinner(R.id.spG, R.array.stG);
        spG.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                selectedSpinnerByStationId(5000+position, spG, 2);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) { parent.setSelection(0);}
        });

        ids = new int[] {
            R.id.part, R.id.YY, R.id.MM, R.id.DD, R.id.SCP_X, R.id.SCP_Y, R.id.SCP_H
        };
        reset1();
        restore();
    }
    private boolean readScp(BufferedReader bufferedReader) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) return false;
        String[] res = line.split(",");
        if (res.length == 4 && res[0].trim().equals("SCP")) {
            ets.get(4).setText(res[1]);
            ets.get(5).setText(res[2]);
            ets.get(6).setText(res[3]);
            return true;
        }
        return false;
    }
    private boolean readP(BufferedReader bufferedReader) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) return false;
        String[] res = line.split(",");
        if (res.length == 2 && res[0].trim().equals("P")) {
            ets.get(7).setText(res[1]);
            return true;
        }
        return false;
    }
    private boolean readCamp(BufferedReader bufferedReader) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null || line.trim().length() == 0) return false;
        String[] res = line.split(",");
        if (res.length == 2 && res[0].trim().equals("CAMP")) {
            ets.get(0).setText(res[1]);
            return true;
        }
        return false;
    }
    private void addLL(int s, int e, String A, String D, String V) {
        EditText etA, etD, etV;
        if (e == 1013) { // 閉塞路徑, 請見 addLlClosedPath()
            return;
        }

        if (getStation(s) == null) stations.add(new Station(s));
        if (getStation(e) == null) stations.add(new Station(e));
        if (e % 1000 < 100) { // 就算是陣地中心，也是一通先加自己
            if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s)+"至"+getStationName(e)+"垂直角是否有誤");
            addLinearLayout(s, e);
        }
        if (1200 < e && e < 1400) addLinearLayout2(e-200);
        else if (3200 < e && e < 3400) addLinearLayout2(e-200);
        else if (5200 < e && e < 5400 && null == getStation(e+200)) { // 要判斷 L 是否已經存在, 不存在再加
            addLl4GaByStationId(s, e);
        }

        int eta;
        if (5200 < e && e < 5400) {
            eta = ets.size()-6;
            etA = ets.get(eta);
            etD = ets.get(eta+1);
            etV = ets.get(eta+2);
            if (etA != null) etA.setText(A);
            if (etD != null) etD.setText(D);
            if (etV != null) etV.setText(V);
        } else {
            eta = ets.size() - 3;
            etA = ets.get(eta);
            etD = ets.get(eta + 1);
            etV = ets.get(eta + 2);
            if (etA != null) etA.setText(A);
            if (etD != null) etD.setText(D);
            if (etV != null) etV.setText(V);
        }
    }
    private void addLlClosedPath(int s, String A, String D) { // 閉塞路徑
        if (getStation(s) == null) return;
        closedPath = new ArrayList<>();
        closedPath.add(s);
        Station S = getStation(s);
        while (S != null && S.id != 1001 && S.sources.size() > 0) {
            closedPath.add(S.id);
            int sourceIdOfS = S.sources.get(0); // 只要 S.id 不是 SCP(1001)，必定要有 source
            EndPoint epS = getStation(sourceIdOfS).getTarget(S.id); // 目前只能從 source 取得自己(source 的目標)的 EndPoint
            if (!(4000 < epS.id && epS.id < 5000) &&
                    (epS.A().trim().isEmpty() || epS.D().trim().isEmpty() || epS.V().trim().isEmpty())) {
                Warn("閉塞路徑無法回溯至統制點，請檢查閉塞路徑上測站『"+getStationName(epS.id)+ "』的各項資料是否都有輸入");
            }
            S = getStation(sourceIdOfS);
        }
        if (S == null) {
            closedPath = null;
            Warn("閉塞路徑無法回溯至統制點，請檢查閉塞路徑("+getStationName(s)+")，並重新選擇測站當閉塞路徑終站");
        } else {
            Spinner spM;
            if ((spM = getSpinnerByStationId(1013)) != null) {
                spM.setBackground(spinnerBG);
            }
            changeButtonColor(btAdd, 0);
            closedPath.add(1001);
            addclosedPath(A, D);
        }
    }
    private boolean readLL(BufferedReader bufferedReader) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) return false;
        line = line.trim();
        if (line.length() == 0 || line.charAt(0) == '#') return true;
        String[] res = line.split(",");
        if (res.length >= 2) {
            int start = Tools.parseInt(res[0]);
            int end = Tools.parseInt(res[1]);
            if (end == 1013) // 閉塞路徑
                addLlClosedPath(start, res[2].trim(), res[3].trim());
            else if (res.length == 2)
                addLL(start, end, "", "", "");
            else if (res.length == 3)
                addLL(start, end, res[2].trim(), "", "");
            else if (res.length == 4)
                addLL(start, end, res[2].trim(), res[3].trim(), "");
            else if (res.length == 5)
                addLL(start, end, res[2].trim(), res[3].trim(), res[4].trim());
            return true;
        }
        return false;
    }
    private void restore() {
        File file = new File(Environment.getExternalStorageDirectory()+"/save.txt");
        try {
            FileReader f = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(f);
            readCamp(bufferedReader);
            readScp(bufferedReader);
            readP(bufferedReader);
            while (readLL(bufferedReader)) {}
            bufferedReader.close();
            f.close();

            spSV.post(new Runnable() {
                @Override
                public void run() {
                    spSV.fullScroll(ScrollView.FOCUS_DOWN);
                    ets.get(ets.size()-1).requestFocus();
                }
            });
        } catch (IOException e) {
            Warn("save.txt 讀檔錯誤，請試著按計算存檔，若持續發生問題時，請通知合作廠商");
        }
    }
    private EditText addclosedPath(String X, String Y) {
        final Station e = getStation(closedPath.get(0));
        final String closedPathStr = String.format(Locale.CHINESE, "閉塞路徑終點測站:%s",e.name);
        lastBtn = new Button(mainActivity); lastBtn.setLayoutParams(btP.getLayoutParams());
        lastBtn.setText(closedPathStr);
        lastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                builder
                        .setMessage("確定要刪除"+closedPathStr+"嗎？")
                        .setPositiveButton("是",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                removeclosedPath();
                                save();
                                reset1();
                                restore();
                            }
                        })
                        .setNegativeButton("否", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });
        lastLL = new LinearLayout(mainActivity); lastLL.setOrientation(LinearLayout.HORIZONTAL);
        lastLL.addView(lastBtn);
        spLL.addView(lastLL);

        final EditText etX = addInputField("橫坐標", X);
        EditText etY = addInputField("縱坐標", Y);
        Station sp = getStation(1013);
        if (sp == null) {
            sp = new Station(1013);
            stations.add(sp);
        }
        ll.add(new LL(e.id, 1013, etX, etY, null, lastLL));
        spSV.post(new Runnable() {
            @Override
            public void run() {
                spSV.fullScroll(ScrollView.FOCUS_DOWN);
                etX.requestFocus();
            }
        });
        return etX;
    }
    List<Integer> closedPath=null;
    boolean closedPathAdded = false;
    private void selectedSpinnerByStationId(int stationId, Spinner sp, int type) {
        if (5012 < stationId && stationId < 5200) stationId += -12+200;
        if (setSource <= 0) {
            Warn("請先點選上方的『+』鈕, 再操作一次下拉選單，以便增加起始點");
        } else if (setSource == 3) { // 閉塞路徑
            if (stationId == 1001 || stationId == 1002 || stationId == 1013) {
                removeclosedPath();
                Warn("閉塞路徑終站不能選擇統制點、統制點至P點、或閉塞路徑本身");
            } else {
                Station c = getStation(stationId);
                EndPoint ep;
                if (c == null || (ep = getStation(c.sources.get(0)).getTarget(c.id)) == null || !(4000 < ep.id && ep.id < 5000) && (ep.D().trim().isEmpty() || ep.V().trim().isEmpty())) {
                    removeclosedPath();
                    Warn("閉塞路徑終站不存在或設定有誤，請確認後重新選擇終站");
                } else
                    addLlClosedPath(stationId, "", "");
            }
        } else if (setSource == 1) { // 本次是設定 sources
            if (stationId == 1013) { // 閉塞路徑
                if (closedPath != null) {
                    if (closedPathAdded) removeclosedPath();
                    else {
                        closedPathAdded = true;
                        Warn("請先移除現有閉塞路徑，再操作新增");
                        sp.setBackground(spinnerBG);
                        changeButtonColor(btAdd, 0);
                    }
                } else {
                    setSource = 3;
                    lastSpinner = sp;
                    sp.setBackgroundColor(Color.GREEN);
                    lastSpInStationId = stationId;
                }
            } else {
                setSource = 2;
                lastSpinner = sp;
                sp.setBackgroundColor(Color.GREEN);
                lastSpInStationId = stationId;
                if (getStationPos(stationId) < 0) stations.add(new Station(stationId));
            }
        } else if (setSource == 2) { // 設定本次的 target
            if (stationId == 1013) {
                sp.setSelection(0);
                Warn("閉塞路徑非測站，不能設為終點\n請另選測站，或按『+』取消");
                return;
            } else if (stationId == lastSpInStationId) {
                sp.setSelection(0);
                Warn("終點測站不能與來源測站相同\n請另選測站，或按『+』取消");
                return;
            } else if (stationId < 1003) {
                sp.setSelection(0);
                Warn("終點測站不能設定為統制點\n請另選測站，或按『+』取消");
                return;
            }
            lastSpinner.setBackground(spinnerBG);
            changeButtonColor(btAdd, 0);
            Station s = getStation(stationId);
            if (s != null && !(4000 < stationId && stationId < 5000) && s.sources.size() > 0) {
                Warn(String.format("%s已存在，請確認後再進行操作", getStationName(stationId)));
            } else if (getLL(lastSpInStationId, stationId) != null) {
                Warn(String.format("%s至%s已存在，建議直接修改舊資料", getStationName(lastSpInStationId), getStationName(stationId)));
            } else {
                final EditText etA;
                // TODO: wade, 如果單獨增加砲陣地，一定要先判斷是否已存在，若是則先刪除舊的
                if (stationId > 5012) { //  陣地選擇點 5013--5024
                    if (getStation(stationId-12+200) != null) {
                        Warn(getStationName(stationId-12+200)+"已存在，若欲新增它，請先刪除舊資料");
                    } else if (getStation (stationId-12+200) == null) { // 加入陣地中心，也許陣地中心是在選擇點之後
                        etA = addLl4GaByStationId(lastSpInStationId, stationId);
                        lastEditText = ets.get(ets.size() - 6);
                        spSV.post(new Runnable() {
                            @Override
                            public void run() {
                                spSV.fullScroll(ScrollView.FOCUS_DOWN);
                                etA.requestFocus();
                            }
                        });
                    }
                } else { // 正常測站
                    etA = addLinearLayout(lastSpInStationId, stationId);
                    if (etA != null) {
                        if (type == 1) { // O
                            addLinearLayout2(stationId);
                            lastEditText = ets.get(ets.size() - 3);
                        } else if (type == 2 && getStation(stationId+200) == null) { // G
                            addLl4GaByStationId(stationId, stationId+200);
                            lastEditText = ets.get(ets.size() - 6);
                        } else {
                            lastEditText = ets.get(ets.size() - 1);
                        }
                        spSV.post(new Runnable() {
                            @Override
                            public void run() {
                                spSV.fullScroll(ScrollView.FOCUS_DOWN);
                                etA.requestFocus();
                            }
                        });
                    } else Warn("selectedSpinnerByStationId(" + stationId + ") etA is null");
                }
            }
        }
        sp.setSelection(0);
    }
    private void changeButtonColor(Button button, int pressed) {
        setSource = pressed;
        if (pressed == 1) {
            button.setTextColor(Color.RED);
            button.setBackgroundColor(Color.CYAN);
        } else {
            button.setTextColor(Color.BLACK);
            button.setBackground(buttonBG);
        }
    }
    private Spinner getSpinnerByStationId(int s) {
        switch (s/1000) {
            case 1: return spM;
            case 2: return spTP;
            case 3: return spO;
            case 4: return spT;
            case 5: return spG;
        }
        return null;
    }
    private String getSpinnerItemTextByStationId(int station) {
        String text = "無測站"+station;
        switch (station/1000) {
            case 1: text = (String)spM.getItemAtPosition(station%100); break;
            case 2: text = (String)spTP.getItemAtPosition(station%100); break;
            case 3: text = (String)spO.getItemAtPosition(station%100); break;
            case 4: text = (String)spT.getItemAtPosition(station%100); break;
            case 5:
                if (5200 < station && station < 5400) {
                    text = (String) spG.getItemAtPosition(station - (5000 -12 + 200));
                } else {
                    text = (String) spG.getItemAtPosition(station % 100);
                }
                break;
        }
        return text;
    }
    private boolean checkclosedPath(int s) {
        boolean r = closedPath != null;
        boolean e = false;

        if (r) {
            for (int i : closedPath) {
                if (getStation(i) == null) {
                    r = false;
                    break;
                }
                if (s == i) e = true;
            }
            if (s <= 0) e = true;
        }
        return r || e;
    }
    private void removeLLItem(int s, int e) {
        if (e == 1013) {
            closedPathAdded = false;
            closedPath = null;
        }
        Station S = getStation(s);
        for (LL l: ll) {
            if (l.start == s && l.end == e) {
                View v = l.linearLayout;
                ((ViewGroup) v.getParent()).removeView(v);
                ll.remove(l);
                if (S != null)
                    S.rmTarget(e);
                rmStation(e);
                break;
            }
        }
    }
    private void removeclosedPath() {
        if (checkclosedPath(1001)) {
            removeLLItem(closedPath.get(0), 1013);
        }
        closedPathAdded = false;
        closedPath = null;
    }
    private EditText addLl4GaByStationId(int s, int e) {
        if (s > 5012 && s <= 5024) s += -12 + 200;
        if (e > 5012 && e <= 5024) e += -12 + 200;
        final int ee=e, ss=s;
        final String stS, stE;

        stS = getSpinnerItemTextByStationId(s);
        stE = getSpinnerItemTextByStationId(e);
        lastBtn = new Button(mainActivity); lastBtn.setLayoutParams(btP.getLayoutParams());
        lastBtn.setText(stS+"至"+stE);
        lastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setSource == 3)
                    Warn("請按上方各種測站下拉式選單選擇閉塞路徑終站");
                else {
                    final Button thisButton = (Button) v;
                    AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                    String st = thisButton.getText().toString();
                    st = st.substring(st.indexOf("至") + 1);
                    builder.setTitle("測站管理")
                            .setMessage("請小心並確認針對測站" + st + "進行的動作:")
                            .setPositiveButton("刪除此測站", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    if (checkclosedPath(ee)) {
                                        removeclosedPath();
                                    }
                                    removeLLItem(ss, ee);
                                    if (getStation(ee+200) != null)
                                        removeLLItem(ee, ee + 200);
                                    save();
                                    reset1();
                                    restore();
                                }
                            })
                            .setNegativeButton("返回並取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            }
        });

        lastLL = new LinearLayout(mainActivity); lastLL.setOrientation(LinearLayout.HORIZONTAL);
        lastLL.addView(lastBtn);
        spLL.addView(lastLL);

        ///
        if (getStationPos(e) < 0) stations.add(new Station(e));
        EditText etA = addInputField("水平角", "");
        EditText etD = addInputField("距離", "");
        EditText etV = addInputField("垂直角", "");
        getStation(s).setTarget(e, etA, etD, etV);
        EditText res = etA;

        ll.add(new LL(s, e, etA, etD, etV, lastLL));

        /////////////////////////////////////
        int ee2=e+200; // L

        lastBtn = new Button(mainActivity); lastBtn.setLayoutParams(btP.getLayoutParams());
        lastBtn.setText(String.format(Locale.CHINESE, "%s至方向基線一端", stE));
        lastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setSource == 3)
                    Warn("請按上方各種測站下拉式選單選擇閉塞路徑終站");
                else
                    Warn("請按上方『...至"+stS+"』按鈕或『"+stS+"至陣地選擇點』按鈕刪除此測站");
            }
        });

        lastLL = new LinearLayout(mainActivity); lastLL.setOrientation(LinearLayout.HORIZONTAL);
        lastLL.addView(lastBtn);
        spLL.addView(lastLL);

        ///
        if (getStationPos(ee2) < 0) {
            stations.add(new Station(ee2));
        }
        etA = addInputField("水平角", "");
        etD = addInputField("距離", "");
        etV = addInputField("垂直角", "");
        getStation(e).setTarget(ee2, etA, etD, etV);

        ll.add(new LL(e, ee2, etA, etD, etV, lastLL));
        return res;
    }
    // O
    private void addLinearLayout2(int s) {
        final String stS;
        int ee=s+200;

        stS = getSpinnerItemTextByStationId(s);

        lastBtn = new Button(mainActivity); lastBtn.setLayoutParams(btP.getLayoutParams());
        if (s < 2000) { // P#
            int p = s - 1000 - 2;
            lastBtn.setText(stS + "至P"+p);
        }
        else
            lastBtn.setText(String.format(Locale.CHINESE, "%s至方位基準點", stS));

        lastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setSource == 3)
                    Warn("請按上方各種測站下拉式選單選擇閉塞路徑終站");
                else
                    Warn("請按上方『...至"+stS+"』按鈕刪除測站");
            }
        });

        lastLL = new LinearLayout(mainActivity); lastLL.setOrientation(LinearLayout.HORIZONTAL);
        lastLL.addView(lastBtn);
        spLL.addView(lastLL);

        ///
        if (getStationPos(ee) < 0) {
            stations.add(new Station(ee));
        }
        EditText etA = addInputField("水平角", "");
        EditText etD = addInputField("距離", "");
        EditText etV = addInputField("垂直角", "");
        getStation(s).setTarget(ee, etA, etD, etV);

        ll.add(new LL(s, ee, etA, etD, etV, lastLL));
    }
    boolean isObserveVector = false;
    Button lastOVButton=null;
    int stOVStart =0, stOVEnd = 0;

    private EditText addLinearLayout(int s, int e) {
        String stS, stE;

        if (s == e) return null;
        if (s > 5012 && s <= 5024) s += -12 + 200;
        if (e > 5012 && e <= 5024) e += -12 + 200;
        final int ss=s, ee=e;
        stS = getSpinnerItemTextByStationId(ss);
        stE = getSpinnerItemTextByStationId(ee);

        // addButton()
        lastBtn = new Button(mainActivity); lastBtn.setLayoutParams(btP.getLayoutParams());
        lastBtn.setText(stS+"至"+stE);
        lastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setSource == 3)
                    Warn("請按上方各種測站下拉式選單選擇閉塞路徑終站");
                else {
                    final Button thisButton = (Button) v;
                    if (isObserveVector) {
                        isObserveVector = false;
                        stOVEnd = ee;
                        TextView textView = new TextView(mainActivity);
                        String res;
                        if (stations == null) return;
                        if (!calc(0)) return;
                        Station S = getStation(stOVStart);
                        Station E = getStation(stOVEnd);
                        double[] pol = Tools.POLd(E.y - S.y, E.x - S.x);
                        res = String.format(Locale.CHINESE, "%s(%.2f,%.2f,%.2f)至%s(%.2f,%.2f,%.2f) 距離 %.2f, 方位角=%s",
                                getStationName(stOVStart), S.x, S.y, S.h,
                                getStationName(stOVEnd), E.x, E.y, E.h,
                                pol[0], Tools.ang2Str(pol[1]));
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        textView.setText(res);
                        textView.setScroller(new Scroller(mainActivity));
                        textView.setVerticalScrollBarEnabled(true);
                        textView.setMovementMethod(new ScrollingMovementMethod());
                        AlertDialog dialog = new AlertDialog.Builder(mainActivity)
                                .setTitle("觀測站資訊")
                                .setView(textView)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();
                        dialog.getWindow().setLayout(size.x, size.y);
                        lastOVButton.setBackground(buttonBG);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                        String st = thisButton.getText().toString();
                        st = st.substring(st.indexOf("至") + 1);
                        builder.setTitle("測站管理")
                                .setMessage("請小心並確認針對測站" + st + "進行的動作:")
                                .setPositiveButton("刪除此測站", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        removeLLItem(ss, ee);
                                        if (getStation(ee+200) != null) {
                                            removeLLItem(ee, ee + 200);
                                            if (getStation(ee + 400) != null)
                                                removeLLItem(ee + 200, ee + 400);
                                        }
                                        save();
                                        reset1();
                                        restore();
                                    }
                                })
                                .setNegativeButton("返回並取消", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                                .setNeutralButton("觀測另一測站", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        isObserveVector = true;
                                        stOVStart = ee;
                                        thisButton.setBackgroundColor(Color.RED);
                                        lastOVButton = thisButton;
                                        dialog.cancel();
                                        Warn("請繼續選擇另一觀測站按鈕");
                                    }
                                })
                                .show();
                    }
                }
            }
        });
        lastLL = new LinearLayout(mainActivity); lastLL.setOrientation(LinearLayout.HORIZONTAL);
        lastLL.addView(lastBtn);
        spLL.addView(lastLL);

        if (getStationPos(ee) < 0) {
            stations.add(new Station(ee));
        }
        EditText etA = addInputField("水平角", "");
        EditText etD = addInputField("距離", "");
        EditText etV = addInputField("垂直角", "");

        Station sp = getStation(ss);
        if (sp == null) {
            Warn("增加輸入欄位有誤: 測站識別碼="+ss);
        } else {
            sp.setTarget(ee, etA, etD, etV);
            ll.add(new LL(ss, ee, etA, etD, etV, lastLL));
        }
        return etA;
    }
    int id4EditText = 1;
    private EditText addInputField(String title, String text) {
        TextView tvA = new TextView(mainActivity); tvA.setText(title); tvA.setLayoutParams(tvPA.getLayoutParams());
        EditText etA = new EditText(mainActivity); etA.setLayoutParams(etPA.getLayoutParams()); etA.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etA.setId(id4EditText++);
        etA.setTextSize(12); etA.setText(text);
        lastLL.addView(tvA);lastLL.addView(etA);
        return etA;
    }
    public void onSpBtClick(View view) {
        Button button = (Button)view;
        switch (button.getId()) {
            case R.id.btSpPads:
                Intent intent = new Intent(this, PadsProgram.class);
                startActivity(intent);
                break;
            case R.id.btSPAdd: {
                if (setSource >= 2) {
                    lastSpinner.setBackground(spinnerBG);
                }
                if (setSource == 3) { // 閉塞路徑
                    removeclosedPath();
                }
                changeButtonColor(btAdd, setSource == 0 ? 1 : 0);
                break;
            }
            case R.id.btSPResult: result(); break;
            case R.id.btSPReset: reset(); break;
            case R.id.btSPCalc: calc(1); break;
        }
    }
    private void saveHeadXls(WritableSheet sheet, WritableCellFormat writeFormat, boolean legion) {
        try {
            date = new Date();

            sheet.addCell(new Label(0, row, String.format(Locale.CHINESE, "%s測地成果表 - %d 年 %d 月 %d 日 %d 時",
                saveFileName(ets.get(0).getText().toString(), legion),
                Integer.valueOf(0+ets.get(1).getText().toString()),
                Integer.valueOf(0+ets.get(2).getText().toString()),
                Integer.valueOf(0+ets.get(3).getText().toString()),
                date.getHours()), writeFormat));
            sheet.mergeCells(0,row,9,row);

            ++row;
            sheet.addCell(new Label(0, row, "區分",writeFormat));
            sheet.addCell(new Label(1, row, "橫座標",writeFormat));
            sheet.mergeCells(1,row,2,row);
            sheet.addCell(new Label(3, row, "縱座標",writeFormat));
            sheet.mergeCells(3,row,4,row);
            sheet.addCell(new Label(5, row, "標高",writeFormat));
            sheet.mergeCells(5,row,6,row);
            if (legion) {
                sheet.addCell(new Label(7, row, "方位統制線方位角", writeFormat));
                sheet.mergeCells(7, row, 9, row);
            } else {
                sheet.addCell(new Label(7, row, "觀檢方位角", writeFormat));
                sheet.addCell(new Label(8, row, "方位基準點方位角", writeFormat));
                sheet.mergeCells(8, row, 9, row);
            }
            ++row;
        } catch (WriteException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了");
            ++row;
        }
    }
    private void saveOXls(int id, WritableSheet sheet, WritableCellFormat writeFormat) {
        Station O = getStation(id);
        Station CP= getStation(4001);
        Station S = getStation(id + 200);

        int idx = id - 3000;
        try {
            if (O != null) {
                EndPoint sEp = O.getTarget(id+200);
                sheet.addCell(new Label(0, row, "O" + idx, writeFormat));
                sheet.addCell(new Label(1, row, String.format(Locale.CHINESE, "%.2f", O.x), writeFormat));
                sheet.mergeCells(1, row, 2, row);
                sheet.addCell(new Label(3, row, String.format(Locale.CHINESE, "%.2f", O.y), writeFormat));
                sheet.mergeCells(3, row, 4, row);
                sheet.addCell(new Label(5, row, String.format(Locale.CHINESE, "%.2f", O.h), writeFormat));
                sheet.mergeCells(5, row, 6, row);

                if (CP != null && CP.sources.size() >= 2) {
                    double[] d = Tools.POLd(O.y - CP.y, O.x - CP.x);
                    double p = (d[1] + 180) % 360.0;
                    sheet.addCell(new Label(7, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil(p)), writeFormat));
                } else {
                    sheet.addCell(new Label(7, row, "", writeFormat));
                }
                if (sEp != null && !isEmptyEp(sEp, false))
                    sheet.addCell(new Label(8, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil(S.a)), writeFormat));
                else
                    sheet.addCell(new Label(8, row, "", writeFormat));
                sheet.addCell(new Label(9, row, "", writeFormat));
            } else {
                sheet.addCell(new Label(0,row,"O"+idx, writeFormat));
                sheet.addCell(new Label(1,row, "", writeFormat));
                sheet.mergeCells(1,row,2,row);
                sheet.addCell(new Label(3,row, "", writeFormat));
                sheet.mergeCells(3,row,4,row);
                sheet.addCell(new Label(5,row, "", writeFormat));
                sheet.mergeCells(5,row,6,row);
                sheet.addCell(new Label(7,row, "", writeFormat));
                sheet.addCell(new Label(8,row, "", writeFormat));
                sheet.addCell(new Label(9,row, "", writeFormat));
            }
            ++row;
        } catch (WriteException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了");
            ++row;
        }
    }
    private void saveCpXls(WritableSheet sheet, WritableCellFormat writeFormat) {
        Station CP = getStation(4001);
        if (CP != null && CP.sources.size() >= 2) {
            try {
                sheet.addCell(new Label(0,row, "⊕", writeFormat));
                sheet.addCell(new Label(1,row, String.format(Locale.CHINESE, "%.2f", CP.x), writeFormat));
                sheet.mergeCells(1,row,2,row);
                sheet.addCell(new Label(3,row, String.format(Locale.CHINESE, "%.2f", CP.y), writeFormat));
                sheet.mergeCells(3,row,4,row);
                sheet.addCell(new Label(5,row, String.format(Locale.CHINESE, "%.2f", CP.h), writeFormat));
                sheet.mergeCells(5,row,6,row);
                sheet.addCell(new Label(7,row, "基線", writeFormat));
                sheet.addCell(new Label(8,row, "基線長", writeFormat));
                sheet.addCell(new Label(9,row, "基線方位角", writeFormat));
                ++row;
            } catch (WriteException e) {
                Warn("無法儲存成果表，請檢查是否已經存滿了");
                ++row;
            }
        } else {
            try {
                sheet.addCell(new Label(0,row, "⊕", writeFormat));
                sheet.addCell(new Label(1,row, "", writeFormat));
                sheet.mergeCells(1,row,2,row);
                sheet.addCell(new Label(3,row, "", writeFormat));
                sheet.mergeCells(3,row,4,row);
                sheet.addCell(new Label(5,row, "", writeFormat));
                sheet.mergeCells(5,row,6,row);
                sheet.addCell(new Label(7,row, "基線", writeFormat));
                sheet.addCell(new Label(8,row, "基線長", writeFormat));
                sheet.addCell(new Label(9,row, "基線方位角", writeFormat));
                ++row;
            } catch (WriteException e) {
                Warn("無法儲存成果表，請檢查是否已經存滿了");
                ++row;
            }
        }
    }
    private void saveTXls(int id, boolean w, WritableSheet sheet, WritableCellFormat writeFormat) {
        int[] Oidx1;
        int[] Oidx2;
        int idx = id - 4001;
        if (getStation(3003) != null) {
            Oidx1 = new int[] { 1, 2, 1 };
            Oidx2 = new int[] { 2, 3, 3 };
        } else {
            Oidx1 = new int[] { 1 };
            Oidx2 = new int[] { 2 };
        }
        Station T = getStation(id);
        Station O1=null; if (id-4001 <= Oidx1.length) O1 = getStation(Oidx1[id-4002]+3000);
        Station O2=null; if (id-4001 <= Oidx2.length) O2 = getStation(Oidx2[id-4002]+3000);

        try {
            sheet.addCell(new Label(0,row, String.format(Locale.CHINESE, "T%d", idx), writeFormat));
            if (T != null && T.sources.size() >= 2) {
                sheet.addCell(new Label(1, row, String.format(Locale.CHINESE, "%.2f", T.x), writeFormat));
                sheet.mergeCells(1, row, 2, row);
                sheet.addCell(new Label(3, row, String.format(Locale.CHINESE, "%.2f", T.y), writeFormat));
                sheet.mergeCells(3, row, 4, row);
                sheet.addCell(new Label(5, row, String.format(Locale.CHINESE, "%.2f", T.h), writeFormat));
                sheet.mergeCells(5, row, 6, row);
            } else {
                sheet.addCell(new Label(1,row, "", writeFormat));
                sheet.mergeCells(1,row,2,row);
                sheet.addCell(new Label(3,row, "", writeFormat));
                sheet.mergeCells(3,row,4,row);
                sheet.addCell(new Label(5,row, "", writeFormat));
                sheet.mergeCells(5,row,6,row);
            }
            if (w) { // T4
                sheet.addCell(new Label(7, row, "方向基線方位角", writeFormat));
                sheet.addCell(new Label(8, row, "砲檢方位角", writeFormat));
                sheet.addCell(new Label(9, row, "方向基角", writeFormat));
            } else if (O1 != null && O2 != null && id < 4005) {
                double[] O1O2=Tools.POLd(O2.y-O1.y, O2.x-O1.x);
                sheet.addCell(new Label(7, row, String.format(Locale.CHINESE, "O%d-O%d", O1.id-3000, O2.id-3000), writeFormat));
                sheet.addCell(new Label(8, row, String.format(Locale.CHINESE, "%.2f", O1O2[0]), writeFormat));
                sheet.addCell(new Label(9, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil(O1O2[1])), writeFormat));
            } else {
                sheet.addCell(new Label(7, row, "", writeFormat));
                sheet.addCell(new Label(8, row, "", writeFormat));
                sheet.addCell(new Label(9, row, "", writeFormat));
            }
        } catch (WriteException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了");
        }
        ++row;
    }
    private void saveGXls(int id, WritableSheet sheet, WritableCellFormat writeFormat) {
        Station G = getStation(id);
        if (G == null) return;
        int idx = id-5000;
        // G1=1, G2=4, G3=7, G4=10
        // G1.1 = 2, G1.2 = 3, G2.1 = 5, G2.2 = 6, G3.1 = 8, G3.2 = 9, G4.1 = 11, G4.2=12
        int a=(idx+2)/3, b=(idx-1)%3;
        Station A = getStation(id + 200);
        Station L = getStation(id + 400);
        Station CP = getStation(4001);
        try {
            if (A != null && L != null) {
                Station sOfA = getStation(A.sources.get(0)); // 有兩種可能，一種是 G, 一種是 來自別的測站
                EndPoint aEp = sOfA.getTarget(G.id+200);
                EndPoint lEp = A.getTarget(id+400);
                if (b == 0) {
                    sheet.addCell(new Label(0, row, String.format(Locale.CHINESE, "G%d", a), writeFormat));
                }
                else {
                    sheet.addCell(new Label(0, row, String.format(Locale.CHINESE, "G%d.%d", a, b), writeFormat));
                }
                sheet.addCell(new Label(1, row, String.format(Locale.CHINESE, "%.2f", G.x), writeFormat));
                sheet.mergeCells(1, row, 2, row);
                sheet.addCell(new Label(3, row, String.format(Locale.CHINESE, "%.2f", G.y), writeFormat));
                sheet.mergeCells(3, row, 4, row);
                sheet.addCell(new Label(5, row, String.format(Locale.CHINESE, "%.2f", G.h), writeFormat));
                sheet.mergeCells(5, row, 6, row);
                if (!isEmptyEp(lEp, false) && !isEmptyEp(aEp, false))
                    sheet.addCell(new Label(7, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil((L.p+180)%360)), writeFormat));
                else sheet.addCell(new Label(7, row, "", writeFormat));
                if (CP != null && CP.sources.size() >= 2) {
                    double a1 = Tools.azimuth(G.x, G.y, CP.x, CP.y);
                    sheet.addCell(new Label(8, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil(a1)), writeFormat));
                    if (!isEmptyEp(lEp, false) && !isEmptyEp(aEp, false)) {
                        double a2 = (L.p - a1 + 360.0) % 360.0;
                        if (a2 >= 180.0) a2 -= 180.0;
                        sheet.addCell(new Label(9, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil(a2)), writeFormat));
                    } else {
                        sheet.addCell(new Label(9, row, "", writeFormat));
                    }
                } else {
                    sheet.addCell(new Label(8, row, "", writeFormat));
                    sheet.addCell(new Label(9, row, "", writeFormat));
                }
            } else {
                if (b == 0) {
                    Log.d(TAG, "A == null || L == null, return");
                    return;
                }
                else
                    sheet.addCell(new Label(0, row, String.format(Locale.CHINESE, "G%d.%d", a, b), writeFormat));

                sheet.addCell(new Label(1,row, "", writeFormat));
                sheet.mergeCells(1,row,2,row);
                sheet.addCell(new Label(3,row, "", writeFormat));
                sheet.mergeCells(3,row,4,row);
                sheet.addCell(new Label(5,row, "", writeFormat));
                sheet.mergeCells(5,row,6,row);
                sheet.addCell(new Label(7,row, "", writeFormat));
                sheet.addCell(new Label(8,row, "", writeFormat));
                sheet.addCell(new Label(9,row, "", writeFormat));
            }
            ++row;
        } catch (WriteException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了");
            ++row;
        }
    }
    private void saveScpNXls(int id, WritableSheet sheet, WritableCellFormat writeFormat) {
        Station ScpN = getStation(id);
        if (ScpN == null) return;
        int idx = id - 1002;
        try {
            sheet.addCell(new Label(0, row, "SCP" + idx, writeFormat));
            sheet.addCell(new Label(1, row, String.format(Locale.CHINESE, "%.2f", ScpN.x), writeFormat));
            sheet.mergeCells(1, row, 2, row);
            sheet.addCell(new Label(3, row, String.format(Locale.CHINESE, "%.2f", ScpN.y), writeFormat));
            sheet.mergeCells(3, row, 4, row);
            sheet.addCell(new Label(5, row, String.format(Locale.CHINESE, "%.2f", ScpN.h), writeFormat));
            sheet.mergeCells(5, row, 6, row);

            EndPoint ScpP = ScpN.getTarget(id+200);
            Station P = getStation(id+200);
            if (ScpP != null && !isEmptyEp(ScpP, false)) {
                sheet.addCell(new Label(7, row, String.format(Locale.CHINESE, "%.2f", Tools.Deg2Mil(P.a)), writeFormat));
                sheet.addCell(new Label(8, row, String.format(Locale.CHINESE, "%s", Tools.Deg2DmsStr(P.a)), writeFormat));
            } else {
                sheet.addCell(new Label(7, row, "", writeFormat));
                sheet.addCell(new Label(8, row, "", writeFormat));
            }
            sheet.addCell(new Label(9, row, "", writeFormat));
            ++row;
        } catch (WriteException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了");
            ++row;
        }
    }
    private void saveOtherXls(WritableSheet sheet, WritableCellFormat writeFormat, boolean legion) {
        Station SCP = getStation(1001);
        try {
            if (!legion) {
                for (int i = 1; i <= 4; i++) {
                    sheet.addCell(new Label(0, row, "迫砲" + i, writeFormat));
                    sheet.addCell(new Label(1, row, "", writeFormat));
                    sheet.mergeCells(1, row, 2, row);
                    sheet.addCell(new Label(3, row, "", writeFormat));
                    sheet.mergeCells(3, row, 4, row);
                    sheet.addCell(new Label(5, row, "", writeFormat));
                    sheet.mergeCells(5, row, 6, row);
                    sheet.addCell(new Label(7, row, "", writeFormat));
                    sheet.addCell(new Label(8, row, "", writeFormat));
                    sheet.addCell(new Label(9, row, "", writeFormat));
                    ++row;
                }
            }

            for (int i=1; i<=3; i++) {
                boolean empty = true;
                if (legion) {
                    sheet.addCell(new Label(0, row, "", writeFormat));
                } else {
                    Station s = getStation(2000 + i);
                    sheet.addCell(new Label(0, row, "抄圖點" + i, writeFormat));
                    if (s != null && s.x != 0 && s.y != 0 && s.h != 0) {
                        sheet.addCell(new Label(1, row, String.format(Locale.CHINESE, "%.2f", s.x), writeFormat));
                        sheet.mergeCells(1, row, 2, row);
                        sheet.addCell(new Label(3, row, String.format(Locale.CHINESE, "%.2f", s.y), writeFormat));
                        sheet.mergeCells(3, row, 4, row);
                        sheet.addCell(new Label(5, row, String.format(Locale.CHINESE, "%.2f", s.h), writeFormat));
                        sheet.mergeCells(5, row, 6, row);
                        empty = false;
                    }
                }

                if (legion || empty) {
                    sheet.addCell(new Label(1, row, "", writeFormat));
                    sheet.mergeCells(1, row, 2, row);
                    sheet.addCell(new Label(3, row, "", writeFormat));
                    sheet.mergeCells(3, row, 4, row);
                    sheet.addCell(new Label(5, row, "", writeFormat));
                    sheet.mergeCells(5, row, 6, row);
                }
                if (i==1) {
                    sheet.addCell(new Label(7,row,"測地作業精度", writeFormat));
                    sheet.mergeCells(7,row,9,row);
                } else if (i == 2) {
                    sheet.addCell(new Label(7,row,"精度比", writeFormat));
                    sheet.addCell(new Label(8,row,"1/", writeFormat));
                    sheet.mergeCells(8,row,9,row);
                } else if (i == 3) {
                    sheet.addCell(new Label(7,row,"徑誤差", writeFormat));
                    sheet.addCell(new Label(8,row,"", writeFormat));
                    sheet.mergeCells(8,row,9,row);
                }
                ++row;
            }

            sheet.addCell(new Label(0,row, "測地統制點", writeFormat));
            sheet.mergeCells(0,row,0,row+1);

            sheet.addCell(new Label(1,row, "點名", writeFormat));
            sheet.addCell(new Label(2,row, "", writeFormat));
            sheet.addCell(new Label(3,row, "屬性", writeFormat));
            sheet.addCell(new Label(4,row, "測地基準點", writeFormat));
            sheet.addCell(new Label(5,row, "座標系統", writeFormat));
            sheet.addCell(new Label(6,row, "UTM(WGS-84)", writeFormat));
            sheet.mergeCells(6,row,7,row);
            sheet.addCell(new Label(8,row, "∠P", writeFormat));
            sheet.mergeCells(8,row,8,row+1);
            sheet.addCell(new Label(9,row, String.format(Locale.CHINESE, "%.2f",Tools.Deg2Mil(SCP.a)), writeFormat));

            ++row;
            sheet.addCell(new Label(1,row, "X", writeFormat));
            sheet.addCell(new Label(2,row, String.format(Locale.CHINESE, "%.2f", SCP.x), writeFormat));
            sheet.addCell(new Label(3,row, "Y", writeFormat));
            sheet.addCell(new Label(4,row, String.format(Locale.CHINESE, "%.2f", SCP.y), writeFormat));
            sheet.addCell(new Label(5,row, "H", writeFormat));
            sheet.addCell(new Label(6,row, String.format(Locale.CHINESE, "%.2f", SCP.h), writeFormat));
            sheet.mergeCells(6,row,7,row);
            sheet.addCell(new Label(9,row, String.format(Locale.CHINESE, "%s",Tools.Deg2DmsStr(SCP.a)), writeFormat));
            ++row;
        } catch (WriteException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了");
            ++row;
        }
    }
    private boolean isLegion() {
        if (getStation(3001) != null) return false;
        for (int i=4001; i<4008; i++) {
            if (getStation(i) != null) return false;
        }
        for (int i=5001; i<5013; i++) {
            if (getStation(i) != null) return false;
        }
        for (int i=1003; i<1013; i++) {
            if (getStation(i) != null) return true;
        }
        return false;
    }
    private String saveFileName(String name, boolean legion) {
        return legion ? "軍團砲兵"+name : "砲兵"+name+"營";
    }
    private void saveXls() {
        date = new Date();
        row = 0;
        boolean legion = isLegion();
        String filename = String.format(Locale.CHINESE, Environment.getExternalStorageDirectory()+"/%s測地成果表-%d%02d%02d-%02d%02d%02d.xls",
                saveFileName(ets.get(0).getText().toString(),legion),
                Integer.valueOf(0+ets.get(1).getText().toString()),
                Integer.valueOf(0+ets.get(2).getText().toString()),
                Integer.valueOf(0+ets.get(3).getText().toString()),
                date.getHours(), date.getMinutes(), date.getSeconds());
        File file = new File(filename);
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(Locale.CHINESE);
        WritableWorkbook workbook;

        try {
            workbook = Workbook.createWorkbook(file, wbSettings);
            WritableSheet sheet = workbook.createSheet("Data", 0);
            CellView cellView = new CellView();
            cellView.setAutosize(true);

            WritableCellFormat writeFormat = new WritableCellFormat();
            writeFormat.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK); // 左邊設粗線
            writeFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK); // 右邊設粗線
            writeFormat.setBorder(Border.TOP, BorderLineStyle.THIN, Colour.BLACK); // 上方框粗線
            writeFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK); // 下方框粗線

            saveHeadXls(sheet, writeFormat, legion);
            if (legion) {
                for (int i=1003; i<1013; i++) {
                    saveScpNXls(i, sheet, writeFormat);
                }
            } else {
                for (int i = 3001; i <= 3003; i++) saveOXls(i, sheet, writeFormat);
                saveCpXls(sheet, writeFormat);
                int lastT = 0;
                for (int i = 4007; i > 4005; i--) {
                    if (getStation(i) != null) {
                        lastT = i;
                        break;
                    }
                }
                if (lastT == 0) lastT = 4005;
                for (int i = 4002; i <= 4016; i++) {
                    if (i > 4005) {
                        if (null != getStation(i))
                            saveTXls(i, i == lastT, sheet, writeFormat);
                    } else
                        saveTXls(i, i == lastT, sheet, writeFormat);
                }
                for (int i = 5001; i <= 5012; i++) saveGXls(i, sheet, writeFormat);
            }
            saveOtherXls(sheet, writeFormat, legion);
            workbook.write();
            workbook.close();
            Warn("請使用從外部電腦取得檢視成果表 "+filename+", 或用檔案管理開啟");
        } catch (IOException e) {
            Warn("無法儲存成果表，請檢查是否已經存滿了: "+filename);
        } catch (WriteException e) {
            Warn("格式有誤");
        }
    }
    private String getStationName(int id) {
        String name = getSpinnerItemTextByStationId(id);
        name = name.replaceAll("選擇點", "陣地選擇點");
        if (1202 < id && id < 1213) { // SCP#
            name += "至P"+(id-1200-2);
        } else if (3200 < id && id < 4000) {
            name += "至方位基準點";
//        } else if (5200 < id && id < 5400) {
//            name += "陣地選擇點";
        } else if (id > 5400) {
            name += "方向基線";
        }
        return name;
    }
    private String addRow(String t1, String t2) {
        return "<tr><td>"+t1+"</td><td>"+t2+"</td></tr>";
    }
    private String addRowStation(Station s, String name) {
        if (name != null) {
            return addRow(getStationName(s.id),
                    String.format(Locale.CHINESE, "坐標、標高=(%.2f, %.2f, %.2f)<br />%s方位角=%s",
                            s.x, s.y, s.h, name, Tools.ang2Str(s.a)));
        } else {
            return addRow(getStationName(s.id),
                    String.format(Locale.CHINESE, "坐標、標高=(%.2f, %.2f, %.2f)",
                            s.x, s.y, s.h));
        }
    }
    // 觀測所
    private String addRowStation3(Station s) {
        String ss = getStationName(s.sources.get(0));
        Station S = getStation(s.id+200);
        EndPoint sEp = s.getTarget(s.id + 200);
        if (sEp != null && !isEmptyEp(sEp, false)) // TODO
            return addRow(s.name,
                String.format(Locale.CHINESE, "坐標、標高=(%.2f, %.2f, %.2f)<br />%s方位角=%s<br/>方位基準點方位角=%s",
                        s.x, s.y, s.h, ss+"至"+s.name, Tools.ang2Str(s.a), Tools.ang2Str(S.a)));
        else
            return addRow(s.name,
                String.format(Locale.CHINESE, "坐標、標高=(%.2f, %.2f, %.2f)<br />%s方位角=%s",
                        s.x, s.y, s.h, ss+"至"+s.name, Tools.ang2Str(s.a)));
    }
    private String getCamp() {
        String camp = ets.get(0).getText().toString();
        if (camp.trim().length() == 0) return "";
        if (isLegion()) return "軍團"+camp;
        else return camp+"營";
    }
    private void result() {
        if (calc(0))
            saveXls();
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        removeclosedPath();
        if (ll != null) {
            for (LL l : ll) {
                View v = l.linearLayout;
                ((ViewGroup) v.getParent()).removeView(v);
                Station ss = getStation(l.start);
                if (ss != null) {
                    ss.targets = null;
                    stations.remove(ss);
                }
            }
            stations = null;
        }
        ll = new ArrayList<>();
        ets = new ArrayList<>();
        for (int id : ids) {
            EditText et = (EditText) findViewById(id);
            et.setText("");
            et.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            ets.add(et);
        }
        date = new Date();
        ets.get(1).setText(String.format(Locale.CHINESE, "%d", (date.getYear()-11)));
        ets.get(2).setText(String.format(Locale.CHINESE, "%d", (date.getMonth()+1)));
        ets.get(3).setText(String.format(Locale.CHINESE, "%d", (date.getDate())));
        etPA.setText("");
        // 初始化最重要的資料
        stations = new ArrayList<>();
        stations.add(new Station(1001));
        stations.add(new Station(1002));
        stations.get(0).setTarget(1002, etPA, null, null);
    }
    private boolean isEmptyEp(EndPoint ep, boolean checkAll) {
        if (ep == null) return false;
        if ((ep.id < 2004 || ep.id > 3000) && checkAll) {
            return ep.A().trim().isEmpty() || ep.D().trim().isEmpty() || ep.V().trim().isEmpty();
        } else return ep.A().trim().isEmpty();
    }

    private String dumpEp(EndPoint ep) {
        if (ep == null) return "";
        return getStationName(ep.id)+"(A="+ep.A()+", D="+ep.D()+", V="+ep.V()+")";
    }
    private void Warn(String msg) {
        Log.i(TAG, msg);
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
    // PADS: 方位確認系統 Position and Azimuth Determining System： s1, ep1 屬於 O1, s2, ep2 屬於 O2/O3
    private void PADS(Station s1, EndPoint ep1, Station s2, EndPoint ep2) {
        Station T = getStation(ep1.id);
        double ep1a = Tools.auto2Deg(ep1.A(), 0);
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s1.id)+"至"+getStationName(ep1.id)+"水平角是否有誤");
        if (ep1a == -10000) return;
        double ep1v = Tools.auto2Deg(ep1.V(), 0);
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s1.id)+"至"+getStationName(ep1.id)+"垂直角是否有誤");
        if (ep1v == -10000) return;
        double ep2a = Tools.auto2Deg(ep2.A(), 0);
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s2.id)+"至"+getStationName(ep2.id)+"水平角是否有誤");
        if (ep2a == -10000) return;
        double ep2v = Tools.auto2Deg(ep2.V(), 0);
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s2.id)+"至"+getStationName(ep2.id)+"垂直角是否有誤");
        if (ep2v == -10000) return;
        T.a  = (ep1a + s1.p + 360)%360;

        double[] res = Tools.Pads(s1.x, s1.y, s1.h, s1.p, ep1a, ep1v, s2.x, s2.y, s2.h, s2.p, ep2a, ep2v);
        if (res != null && res.length >= 3) {
            T.x = res[0];
            T.y = res[1];
            T.h = res[2];
        }
    }

    // 絕大多數都是採用導線法
    private boolean traverse(Station s, EndPoint e) {
        boolean ret = true;
        if (4001 <= e.id && e.id < 5000) return true; // 目標暫時都不計算
        if (3200 < e.id && e.id < 4000) return true;  // S 暫時不計算
        double a, d, v;
        // 先檢查 s --> e 是否都有 ADV
        EndPoint epE = s.getTarget(e.id);
        if (isEmptyEp(epE, true)) {
            Warn("警告：測站『"+getStationName(epE.id)+"』的資料有缺，請檢查");
            return false;
        }
        d = Tools.parseDouble(e.D());
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s.id)+"至"+getStationName(e.id)+"距離是否有誤");
        a = Tools.auto2Deg(e.A(), 0);
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s.id)+"至"+getStationName(e.id)+"方向角是否有誤");
        v = Tools.auto2Deg(e.V(), 0);
        if (Tools.parseDoubleException) Warn("請檢查"+getStationName(s.id)+"至"+getStationName(e.id)+"垂直角是否有誤");
        double[] res = Tools.traverse3(s.x, s.y, s.h, s.p, d, a, v);
        Station t = getStation(e.id);
        t.x = res[0]; t.y = res[1]; t.h = res[2]; t.a = res[3];
        if (t.p <= -10000) t.p = (t.a+180)%360.0;
//        Log.i(TAG, String.format("導線法: (%s->%s) T=(%.2f, %.2f, %.2f, A=%s, p=%s)", s.name, t.name, t.x, t.y, t.h, Tools.ang2Str(t.a), Tools.ang2Str(t.p)));
        for (EndPoint ep : t.targets) {
            if (!isEmptyEp(ep, true)) {
                if (!traverse(t, ep)) ret = false;
            }
        }
        return ret;
    }
    private boolean isValidStation(Station s) {
        return s != null && s.x > -10000.0 && s.y > -10000.0 && s.h > -10000.0;
    }
    private void preview() {
        WebView textView = new WebView(this);
        String res = "<table border='1'>";
        if (stations == null) return;

        // 封閉路徑
        if (closedPath != null && checkclosedPath(0)) {
            if (acct()) {
                Station endP = getStation(closedPath.get(0));
                if (4001 <= endP.id && endP.id <= 4007)
                    res += addRow("徑誤差", String.format(Locale.CHINESE, "%.3f\n", diff));
                else
                    res += addRow("作業精度", String.format(Locale.CHINESE, "1/%s = %.3f(徑誤差) / %.3f(距離總長)\n", ""+(acc==0?"未知":(acc==1000000?"&#8734;":acc)), diff, closedPathLen));
            }
        }

        /* 各測站 */
        Station s = getStation(1001);
        res += addRowStation(s, "統制點至P點");

        // SCP1 .. SCP10
        for (int id=1003; id<1013; id++) {
            if ((s = getStation(id)) == null || s.sources.size() == 0) continue;

            Station P = getStation(id+200);
            String ss = getStationName(s.sources.get(0));
            if (isValidStation(s)) {
                res += addRowStation(s, ss + "至" + s.name);
                if (P != null && P.a != 0) {
                    res += addRow(getStationName(id+200),
                            String.format(Locale.CHINESE, "方位角=%s",
                                    Tools.ang2Str(P.a)));
                }
            }
        }
        // 測站 1 .. 測站 30
        for (int id=2001; id<=2033; id++) {
            s = getStation(id);
            if (s == null) continue;
            if (s.sources.size() <= 0) continue;
            String ss = getStationName(s.sources.get(0));
            if (isValidStation(s))
                res += addRowStation(s, ss+"至"+s.name);
        }
        // 觀測所1..3
        for (int id=3001; id<=3003; id++) {
            s = getStation(id);
            if (s == null) continue;
            if (isValidStation(s))
                res += addRowStation3(s);
        }
        // 觀測所 至 另一觀測所
        Station O1 = getStation(3001);
        Station O2 = getStation(3002);
        Station O3 = getStation(3003);
        if (O1 != null && isValidStation(O1) &&
            O2 != null && isValidStation(O2)) {
            double[] O1O2 = Tools.POLd(O2.y-O1.y, O2.x-O1.x);
            res += addRow(O1.name+"至"+O2.name,
                    String.format(Locale.CHINESE, "距離=%.2f, \t方位角=%s\n",
                            O1O2[0], Tools.ang2Str(O1O2[1])));
        }
        if (O1 != null && isValidStation(O1) &&
            O3 != null && isValidStation(O3)) {
            double[] O1O3 = Tools.POLd(O3.y-O1.y, O3.x-O1.x);
            res += addRow(O1.name+"至"+O3.name,
                    String.format(Locale.CHINESE, "距離=%.2f, \t方位角=%s\n",
                            O1O3[0], Tools.ang2Str(O1O3[1])));
        }
        if (O2 != null && isValidStation(O2) &&
            O3 != null && isValidStation(O3)) {
            double[] O2O3 = Tools.POLd(O3.y-O2.y, O3.x-O2.x);
            res += addRow(O2.name+"至"+O3.name,
                    String.format(Locale.CHINESE, "距離=%.2f, \t方位角=%s\n",
                            O2O3[0], Tools.ang2Str(O2O3[1])));
        }
        // 觀測所看觀測所/目標/檢驗點
        for (int O=3001; O<=3003; O++) {
            Station sO = getStation(O);
            if (sO == null || sO.x <= 0 || sO.y <= 0) continue;
            String nameO = getStationName(O);
            for (int T=4001; T<=4016; T++) {
                Station sT = getStation(T);
                if (!isValidStation(sT)) continue;
                if (sT.sources.size() < 2) continue;
                String nameT=getStationName(T);
                double[] d = Tools.POLd(sT.y-sO.y, sT.x-sO.x);
                res += addRow(nameO +"至"+nameT,
                        String.format(Locale.CHINESE, "距離=%.2f, 方位角=%s\n",
                                d[0], Tools.ang2Str(d[1])));
            }
        }
        // 檢驗點＋目標
        for (int id=4001; id<4200; id++) {
            if ((s = getStation(id)) == null) continue;
            if (s.sources.size() < 2) continue;
            if (isValidStation(s)) {
                res += addRowStation(s, null);
            }
        }
        Station CP = getStation(4001);
        /*
            各陣地中心
            Station sOfA = getStation(A.sources.get(0)); // 舊作法裡，這邊必定是 G
            EndPoint aEp = sOfA.getTarget(i+200);
         */
        for (int id=5001; id<=5012; id++) {
            Station G = getStation(id), S;
            boolean isGExist = true;
            if (G == null || !isValidStation(G)) isGExist = false;
            if (isGExist && (G.sources == null || G.sources.size() == 0)) {
                Warn("無法找到 " + G.id + " 的來源測站, 請檢查測站資訊後再計算");
                continue;
            }

            if (isGExist) {
                S = getStation(G.sources.get(0));
                if (isValidStation(S)) // TODO: check
                    res += addRowStation(G, S.name + "至" + getStationName(G.id));
            }
            Station A = getStation(id + 200);
            Station L = getStation(id + 400);
            if (A != null && L != null) {
                double p = (L.p + 180) % 360;

                Station sOfA = getStation(A.sources.get(0)); // 有兩種可能，一種是 G, 一種是 來自別的測站
                EndPoint aEp = sOfA.getTarget(id+200);

                EndPoint lEp = A.getTarget(id + 400);
                String msg="";
                if (aEp != null && !isEmptyEp(aEp, false)) {
                    if (isValidStation(A)) {
                        msg = String.format(Locale.CHINESE,
                                "坐標、標高=(%.2f, %.2f, %.2f)<BR />",
                                A.x, A.y, A.h);
                    }
                    if (!isEmptyEp(lEp, false))
                        msg += String.format(Locale.CHINESE, "方向基線方位角=%s<BR />", Tools.ang2Str(p));
                }

                if (isGExist && CP != null && CP.sources.size() >= 2) {
                    if (!isEmptyEp(lEp, false)) {
                        double a1 = (L.p - Tools.azimuth(G.x, G.y, CP.x, CP.y) + 360.0) % 360;
                        if (a1 >= 180.0) a1 -= 180;
                        msg += String.format(Locale.CHINESE, "方向基角=%s<br />", Tools.ang2Str(a1));
                        res += addRow(A.name, msg);
                    }
                    double[] d = Tools.POLd(CP.y - G.y, CP.x - G.x);

                    res += addRow(getStationName(G.id) + "至檢驗點",
                            String.format(Locale.CHINESE, "距離=%.2f, 方位角=%s\n",
                                    d[0], Tools.ang2Str(d[1])));
                } else if (msg.length() > 0) res += addRow(A.name, msg);
            } else if (isGExist){
                Warn("預覽有誤: " + getStationName(id) + " 缺 A 或 L");
            }
        }
        // 砲陣地看目標
        for (int G=5001; G<=5012; G++) {
            Station sG = getStation(G);
            if (sG == null || sG.x <= 0 || sG.y <= 0) continue;
            for (int T=4002; T<=4016; T++) {
                Station sT = getStation(T);
                if (sT == null || sT.x <= 0 || sT.y <= 0) continue;
                if (sT.sources.size() < 2) continue;
                double[] d = Tools.POLd(sT.y-sG.y, sT.x-sG.x);
                res += addRow(getStationName(G) +"至"+ getStationName(T),
                        String.format(Locale.CHINESE, "距離=%.2f, \t方位角=%s\n",
                        d[0], Tools.ang2Str(d[1])));
            }
        }
        res += "</table>";
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        WebSettings mWebSettings = textView.getSettings();
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setDefaultFontSize(16);
        textView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        textView.setScrollbarFadingEnabled(false);

        textView.loadData(res, "text/html; charset=utf-8", "UTF-8");
        textView.setVerticalScrollBarEnabled(true);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getCamp()+" 成果快速預覽")
                .setView(textView)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
        dialog.getWindow().setLayout(size.x, size.y);
    }

    private void calc_cleanAllStations() {
        for (Station s : stations) {
            s.x = s.y = s.y = s.a = s.p = s.h = -10000.0;
        }
    }
    private Station calc_checkSCP() {
        Station SCP = stations.get(0);
        if (SCP == null
                || ets.get(4).getText().toString().trim().length() == 0
                || ets.get(5).getText().toString().trim().length() == 0
                || ets.get(6).getText().toString().trim().length() == 0
                || ets.get(7).getText().toString().trim().length() == 0)
        {
            Warn("請先填入統制點、統制點至P點各項數據，再按計算");
            return null;
        }

        SCP.x = Tools.parseDouble(ets.get(4));
        if (Tools.parseDoubleException) {
            Warn("請檢查統制點橫坐標是否有誤");
            return null;
        }
        SCP.y = Tools.parseDouble(ets.get(5));
        if (Tools.parseDoubleException) {
            Warn("請檢查統制點縱坐標是否有誤");
            return null;
        }
        SCP.h = Tools.parseDouble(ets.get(6));
        if (Tools.parseDoubleException) {
            Warn("請檢查統制點標高是否有誤");
            return null;
        }
        SCP.p = Tools.auto2Deg(ets.get(7).getText().toString(), 0);
        if (Tools.parseDoubleException) {
            Warn("請檢查統制點至P點方位角是否有誤");
            return null;
        }
        SCP.a = (SCP.p+360.0)%360.0;
        SCP.p = SCP.a;
        return SCP;
    }

    private boolean calc_traverses(Station SCP) {
        boolean ret = true;
        for (EndPoint e : SCP.targets) {
            if (e.id < 1003) continue;
            if (isEmptyEp(e, true)) {
                Warn("請檢查"+getStationName(e.id)+"是否設定正確");
                continue;
            }
            if (!traverse(SCP, e)) ret = false;
        }
        return ret;
    }
    private void calc_SCPs() {
        for (int i=1003; i<1013; i++) {
            Station s = getStation(i);
            if (s == null) continue;
            for (EndPoint e : s.targets) {
                if (e.id == s.id + 200) {
                    Station se = getStation(e.id);
                    if (!isEmptyEp(e, false)) {
                        se.a = (s.p + Tools.auto2Deg(e.A(), 0) + 360.0) % 360.0;
                        if (Tools.parseDoubleException)
                            Warn("請檢查" + getStationName(i) + "至" + getStationName(e.id) + "水平角是否有誤");
                    } else {
                        Warn("請檢查"+getStationName(e.id)+"是否設定正確");
                        se.a = 0;
                    }
                }
            }
        }
    }
    private void calc_O_S() {
        for (int i=3001; i<=3003; i++) {
            Station O = getStation(i);
            if (O == null) continue;
            Station S = getStation(i + 200);
            if (S != null) {
                EndPoint sEp = O.getTarget(i + 200);
                if (sEp != null) {
                    if (isEmptyEp(sEp, false)) {
                        Warn("請檢查"+getStationName(sEp.id)+"是否設定正確");
                    } else {
                        S.a = (O.p + Tools.auto2Deg(sEp.A(), 0) + 360.0) % 360.0;
                        if (Tools.parseDoubleException)
                            Warn("請檢查"+getStationName(sEp.id)+"是否設定正確");
                        S.p = (S.a + 180) % 360;
                    }
                }
            } else {
                Warn("請注意，缺" + getStationName(i) +"資訊");
            }
        }
    }

    private void calc_TargetPads() {
        for (int i=4001; i<=4016; i++) {
            Station T = getStation(i);
            if (T == null) continue;
            if (T.sources.size() < 2) {
                Warn(getStationName(i)+"的來源測站必須要有兩個站");
                continue;
            }
            Station sO1 = getStation(T.sources.get(0));
            Station sO2 = getStation(T.sources.get(1));
            Station sO3 = T.sources.size() > 2?getStation(T.sources.get(2)):null;
            if (sO1 == null || sO2 == null) {
                Warn("請檢查是否至少2個觀測所資料？");
                continue;
            }
            EndPoint ep1=sO1.getTarget(i), ep2=sO2.getTarget(i);
            EndPoint ep3 = sO3==null?null:sO3.getTarget(i);
            if (ep1 == null || ep2 == null) {
                Warn("並未正確設定"+getStationName(i)+"資訊");
                continue;
            }
            if (isEmptyEp(ep1, false)) {
                Warn("請檢查"+getStationName(ep1.id)+"是否設定正確");
                continue;
            }
            if (isEmptyEp(ep2, false)) {
                Warn("請檢查"+getStationName(ep2.id)+"是否設定正確");
                continue;
            }

            double x=0, y=0, h=0;
            Station t;
            if ((ep1.V().trim().length() == 0 && ep2.V().trim().length() == 0)) {
                Warn(getStationName(i)+"資訊不足:\n"+ getStationName(sO1.id)+"-->"+dumpEp(ep1)+"\n"+getStationName(sO2.id)+"-->"+dumpEp(ep2));
            } else if (ep1.V().trim().length() > 0) {
                PADS(sO1, ep1, sO2, ep2);
                t = getStation(ep2.id);
                x = t.x; y=t.y; h = t.h;
            } else {
                PADS(sO2, ep2, sO1, ep1);
                t = getStation(ep1.id);
                x = t.x; y=t.y; h = t.h;
            }
            // ep3 有兩種，一種是 O1-O2 + O1-O3, 另一種是 O1-O2-O3
            if (ep3 != null) {
                if (isEmptyEp(ep3, false)) {
                    Warn("請檢查"+getStationName(ep3.id)+"是否設定正確");
                } else {
                    PADS(sO1, ep1, sO3, ep3);
                    t = getStation(ep3.id);
                    t.x = (x + t.x) / 2;
                    t.y = (y + t.y) / 2;
                    t.h = (h + t.h) / 2;
                }
            }
        }
    }

    private void calc_G_A_OL() {
        // 針對 砲陣地 進行 OL, A 計算，其中因為可能被切斷，要先接回來
        // TODO: wade, G, A, OL, GO 等應該分開判斷
        // 至少目前陣地中心(G)與選擇點(A)都是獨立的
        for (int i=5001; i<=5012; i++) {
            // 有時候 G 可能是空的(根本沒測量到)
            Station G = getStation(i);
            if (G == null) continue;
            // 但是如果有 G, 就應該要有 A, L
            Station A = getStation(i+200);
            if (A == null) { Warn("請檢查是否已增加"+getStationName(i+200)+"資訊"); continue; }
            Station L = getStation(i+400);
            if (L == null) { Warn("請檢查是否已增加"+getStationName(i+400)+"資訊"); continue; }

            // 因為要打斷 G/A, 所以有兩種可能，一種是 G, 一種是 來自別的測站
            Station sOfA = getStation(A.sources.get(0));
            EndPoint aEp = sOfA.getTarget(A.id);

            Station sOfG = getStation(G.sources.get(0));
            EndPoint gEp = sOfG.getTarget(G.id);
            // 檢查 G
            if (gEp == null || isEmptyEp(gEp, true)) {
                Warn("請檢查"+getStationName(gEp.id)+"是否設定正確");
                continue;
            }
            // 檢查 A
            double Ap = A.p;
            if (aEp == null || isEmptyEp(aEp, false)) { // 基本上這項不會發生
                Warn(getStationName(i+200)+"資訊有誤，請檢查無誤再計算");
                continue;
            } else {
                Ap = (sOfA.p + Tools.auto2Deg(aEp.A(), 0) + 360.0) % 360.0;
                if (Tools.parseDoubleException) {
                    Warn("請檢查"+getStationName(aEp.id)+"是否設定正確");
                    continue;
                }
            }

            EndPoint lEp = A.getTarget(i + 400);
            if (lEp == null || isEmptyEp(lEp, false)) {
                Warn("請檢查是否已增加" + getStationName(i + 400) + "資訊");
                continue;
            } else {
                L.p = (Ap + Tools.auto2Deg(lEp.A(), 0) + 360.0) % 360.0;
                if (Tools.parseDoubleException) {
                    Warn("請檢查" + getStationName(lEp.id) + "是否設定正確");
                    continue;
                }
            }
        }
    }
    private boolean calc(int willPreview) {
        calc_cleanAllStations();
        Station SCP = calc_checkSCP();
        if (SCP == null) return false;

        if (!calc_traverses(SCP)) return false;
        save();
        calc_SCPs();
        calc_O_S();
        calc_TargetPads();
        calc_G_A_OL();

        if (willPreview == 1) {
            preview();
        }
        return true;
    }
    private boolean acct() {
        if (closedPath == null || closedPath.size() == 0) return false;
        Station endP = getStation(closedPath.get(0));
        acc = 0; diff = closedPathLen = 0;

        if (endP == null) return false;
        LL l = getLL(endP.id, 1013);
        if (l == null) return false;
        if (!(4000 < endP.id && endP.id < 5000) && (l.A().trim().isEmpty() || l.D().trim().isEmpty())) {
            Warn("請檢查閉塞路徑各項輸入值是否有誤");
            return false;
        }
        double x2 = Tools.parseDouble(l.A()); // 閉塞路徑是以 A, D 來存 x,y
        if (Tools.parseDoubleException) { Warn("請檢查輸入值是否有誤"); return false; }
        double y2 = Tools.parseDouble(l.D()); // 閉塞路徑是以 A, D 來存 x,y
        if (Tools.parseDoubleException) { Warn("請檢查輸入值是否有誤"); return false; }
        double x1 = endP.x;
        double y1 = endP.y;
        double[] d = Tools.POLd(y2-y1, x2-x1);
        diff = d[0]; // 徑誤差
        ArrayList<Integer> temp = new ArrayList<>(closedPath);
        Collections.reverse(temp); // 讓它從 SCP 出發
        closedPathLen = 0;
        Station e = null;
        Station s = getStation(temp.get(0));
        temp.remove(0);

        for (int i : temp) {
            if (i == s.id) break;
            EndPoint endPoint = s.getTarget(i);
            if (endPoint == null) {
                Warn("閉塞路徑並不連續，請檢查測站『"+ getStationName(s.id)+ "』是否有測量至『"+getStationName(i)+"』或是刪除閉塞路徑");
                return false;
            } else if (!(4000 < endPoint.id && endPoint.id < 5000) &&
                    (endPoint.A().trim().isEmpty() || endPoint.D().trim().isEmpty() || endPoint.V().trim().isEmpty())) {
                Warn("閉塞路徑並不連續，請檢查測站『"+ getStationName(endPoint.id)+ "』正確輸入數值後再計算，或是刪除閉塞路徑");
                return false;
            }
            e = getStation(i);
            if (e != null) {
                closedPathLen += Tools.lengthOfXY(s.x, s.y, e.x, e.y);
                s = e;
            } else {
                Warn("閉塞路徑並不連續2，請檢查測站『"+ getStationName(e.id)+ "』正確輸入數值後再計算，或是刪除閉塞路徑");
                return false;
            }
        }
        if (s == null || e == null) return false;
        if (d[0] > 0) {
            double acc1 = closedPathLen / d[0];
            acc = (int) Math.round(acc1 / 100);
            acc *= 100;
            if (acc <= 0) acc = 1;
        } else acc = 1000000;
        return true;
    }
    private LL getLL(int start, int end) {
        for (LL l : ll) {
            if (l.start == start && l.end == end) return l;
        }
        return null;
    }
    private void save() {
        try {
            File file = new File(Environment.getExternalStorageDirectory()+"/save.txt");
            FileWriter f  = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(f);

            bufferedWriter.write(String.format(Locale.CHINESE, "CAMP,%s\nSCP,%s,%s,%s\nP,%s\n",
                    ets.get(0).getText(),
                    ets.get(4).getText(), ets.get(5).getText(), ets.get(6).getText(), ets.get(7).getText()));
            for (LL l : ll) {
                if (l.end == 1013) { // 閉塞路徑
                    bufferedWriter.write(String.format(Locale.CHINESE, "%d,%d,%s,%s,%s\n",
                            l.start, l.end, l.A(), l.D(), " "));
                } else {
                    bufferedWriter.write(String.format(Locale.CHINESE, "%d,%d,%s,%s,%s\n",
                            l.start, l.end, l.A(), l.D(), l.V()));
                }
            }
            bufferedWriter.flush();
            bufferedWriter.close();
            f.close();
        } catch (IOException e) {
            Warn("save.txt 存檔錯誤，請試著按計算存檔，若持續發生問題時，請通知合作廠商");
        }
    }
}
