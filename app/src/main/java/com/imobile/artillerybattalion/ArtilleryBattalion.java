package com.imobile.artillerybattalion;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.XYZ;

import java.io.File;

public class ArtilleryBattalion extends AppCompatActivity {

    public static XYZ xyz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xyz = new XYZ(this);
        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo=null;
        PackageInfo pInfo = null;
        String version = "1.2.4";
        try {
            pInfo = packageManager.getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // nothing
        }

        try {
            packageInfo = packageManager.getPackageInfo("com.imobile.mt8382hotkeydefine", 0);
        } catch (PackageManager.NameNotFoundException e) {
            if (!(Build.PRODUCT.contains("newmobi6582_wt_l") || Build.PRODUCT.contains("mt82kk_tb") || Build.PRODUCT.contains("pandora"))) {
                try {
                    packageInfo = packageManager.getPackageInfo(this.getPackageName(), 0);
                    long installedTime = packageInfo.lastUpdateTime; // firstInstallTime
                    long now = System.currentTimeMillis();
                    long WpD=8, MpD=30, YpD=365, DpH = 24, HpM = 60, MpS = 60, Spm = 1000;
                    long allowedTime = 7 * DpH * HpM * MpS * Spm;
                    if ((now - installedTime) > allowedTime) {
                        finish();
                    }
                } catch (PackageManager.NameNotFoundException ee) {
                    finish();
                }
            }
        }
        setContentView(R.layout.artillery_battalion);
        TextView appName = (TextView)findViewById(R.id.app_name);
        appName.setText("            "+appName.getText()+"   版本 "+version);

        // TODO: 利用 Build.DISPLAY 來判斷不同的出貨
        if (Build.DISPLAY.toLowerCase().indexOf("school") > 0 || Build.DISPLAY.contains("rk31")) {
            appName.setBackgroundResource(R.color.colorAccent);
        } else {
            appName.setBackgroundColor(Color.BLUE);
        }
    }
    public static XYZ getXYZ() { return xyz; }
    public void onButtonClick(View view){
        final int id = view.getId();
        Intent intent;
        File file;
        switch (id) {
            case R.id.btHelp:
                file = new File("/sdcard/Documents/UserManual.pdf");
                if (!file.exists()) {
                    file = new File("/system/local_script/UserManual.pdf");
                }
                if (!file.exists()) {
                    file = new File("/sdcard/UserManual.pdf");
                }
                if (file.exists()) {
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.fromFile(file),"application/pdf");
                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    intent = Intent.createChooser(target, "Open File");
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "請確認系統中有安裝手冊", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btHelp2:
                file = new File("/sdcard/Documents/OperatorManual.pdf");
                if (!file.exists()) {
                    file = new File("/system/local_script/OperatorManual.pdf");
                }
                if (!file.exists()) {
                    file = new File("/sdcard/OperatorManual.pdf");
                }
                if (file.exists()) {
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.fromFile(file),"application/pdf");
                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    intent = Intent.createChooser(target, "Open File");
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "請確認系統中有安裝手冊", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btSurveyProgram:
                intent = new Intent(this, SurveyProgram.class);
                startActivity(intent);
                break;
            case R.id.btSide3:
                intent = new Intent(this, Side3.class);
                startActivity(intent);
                break;
            case R.id.btHeight:
                intent = new Intent(this, Height.class);
                startActivity(intent);
                break;
            case R.id.btP1AntiIntersection:
                intent = new Intent(this, P1AntiIntersection.class);
                startActivity(intent);
                break;
            case R.id.btP2AntiIntersection:
                intent = new Intent(this, P2AntiIntersection.class);
                startActivity(intent);
                break;
            case R.id.btP3AntiIntersection:
                intent = new Intent(this, P3AntiIntersection.class);
                startActivity(intent);
                break;
            case R.id.btTraverse:
                intent = new Intent(this, Traverse.class);
                startActivity(intent);
                break;
            case R.id.btCelestialBody:
                intent = new Intent(this, CelestialBodySunHeight.class);
                startActivity(intent);
                break;
            case R.id.btCoordinateAdj:
                intent = new Intent(this, CoordinateAdj.class);
                startActivity(intent);
                break;
            case R.id.btCompass:
                intent = new Intent(this, CompassActivity.class);
                startActivity(intent);
                break;
            case R.id.btIntersection:
                intent = new Intent(this, Intersection.class);
                startActivity(intent);
                break;
            case R.id.btAzimuthDistance:
                intent = new Intent(this, AzimuthDistance.class);
                startActivity(intent);
                break;
            case R.id.btCalculator:
                PackageManager pm = getPackageManager();
                String packageName = "com.imobile.calculator";
                try {
                    pm.getPackageInfo(packageName, 0);
                } catch (PackageManager.NameNotFoundException e) {
                    packageName = "com.imobile.calculator2";
                    try {
                        pm.getPackageInfo(packageName, 0);
                    } catch (PackageManager.NameNotFoundException e1) {
                        packageName = "";
                    }
                }
                if (packageName.length() > 0) {
                    intent = pm.getLaunchIntentForPackage(packageName);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "請檢查是否有安裝電子計算機軟體", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btTriangulationSurvey:
                intent = new Intent(this, TriangulationSurvey.class);
                startActivity(intent);
                break;
            case R.id.btSide2Angle:
                intent = new Intent(this, Side2Angle.class);
                startActivity(intent);
                break;
            case R.id.btAngleTools:
                intent = new Intent(this, AngleTools.class);
                startActivity(intent);
                break;
            case R.id.btCoordinateTransform:
                intent = new Intent(this, CoordinateTransform.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
