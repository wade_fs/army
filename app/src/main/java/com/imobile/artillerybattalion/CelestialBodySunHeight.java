package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CelestialBodySunHeight extends Activity {
    public static final String TAG = "MyLog";
    CelestialBodySunHeight mainActivity;
    int ids[];
    EditText ets[];
    Button btCalc, btReset, btDegreeMode, btSwitch, btPdf;
    TextView mTvResult;

    int degreeMode = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.celestial_body_sun_height);
        mainActivity = this;
        ids = new int[] {
                R.id.ett1, R.id.ett2, R.id.ett3,
                R.id.eta1, R.id.eta2, R.id.eta0,
                R.id.etst, R.id.etlt,
                R.id.eth1, R.id.eth2,
                R.id.eth3, R.id.eth4, R.id.eth5, R.id.eth6,
                R.id.etdno, R.id.etdn, R.id.ets,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }

        btSwitch = (Button)findViewById(R.id.btCBDSwitch);
        btSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CelestialBodyPolaris.class);
                startActivity(intent);
            }
        });

        btDegreeMode = (Button)findViewById(R.id.btCBDDegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

        btCalc = (Button)findViewById(R.id.btCBDCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btCBDReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        mTvResult = (TextView)findViewById(R.id.tvCBDResult);

        btPdf = (Button)findViewById(R.id.btPdf);
        btPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyAssets("13.14.pdf");
            }
        });
    }
    private void copyAssets(String pdf) {
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir(), pdf);
        if (!file.exists()) {
            try {
                in = assetManager.open(pdf);
                out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                Log.e("MyLog", e.getMessage());
            }
        }

        Intent pdfReader = new Intent(Intent.ACTION_VIEW);
        pdfReader.setDataAndType(Uri.fromFile(file), "application/pdf");
        pdfReader.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(Intent.createChooser(pdfReader, "Open File"));
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
    private boolean checkInput(int part) {
        if (part == 0) {
            for (int i = 0; i < 10; i++) {
                if (ets[i].getText().length() == 0) return false;
            }
        } else if (part == 1) {
            for (int i = 10; i < ids.length; i++) {
                if (ets[i].getText().length() == 0) return false;
            }
        }
        return true;
    }

    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }
    private void calc() {
        String msg = "";
        double h = 0, t1, t2, t3, a1, a2, a0, st, lt, h1, h2, t, aa;
        double h3, h4, h5, h6, dno, dn, s, z, pd, ss, a, b;
        if (checkInput(0)) {
            t1 = Tools.auto2Deg(ets[0].getText().toString(), degreeMode);
            t2 = Tools.auto2Deg(ets[1].getText().toString(), degreeMode);
            t3 = Tools.auto2Deg(ets[2].getText().toString(), degreeMode);
            a1 = Tools.auto2Deg(ets[3].getText().toString(), degreeMode);
            a2 = Tools.auto2Deg(ets[4].getText().toString(), degreeMode);
            a0 = Tools.auto2Deg(ets[5].getText().toString(), degreeMode);
            st = Tools.auto2Deg(ets[6].getText().toString(), degreeMode);
            lt = Tools.auto2Deg(ets[7].getText().toString(), degreeMode);
            h1 = Tools.auto2Deg(ets[8].getText().toString(), degreeMode);
            h2 = Tools.auto2Deg(ets[9].getText().toString(), degreeMode);
            t = (t1 + t2) / 2 + t3 - 8;
            aa = (a1 + a2 - a0) / 2 + Tools.FIX(a1 / 180.0) * 180.0;
            h = (h2 - h1 - 180.0) / 2;
            msg = "平均高低角=" + Tools.Deg2DmsStr2(h) + "\n";

            if (checkInput(1)) {
                h3 = Tools.parseDouble(ets[10]);
                h4 = Tools.parseDouble(ets[11]);
                h5 = Tools.parseDouble(ets[12]);
                h6 = Tools.parseDouble(ets[13]);
                dno = Tools.auto2Deg(ets[14].getText().toString(), degreeMode);
                dn = Tools.auto2Deg(ets[15].getText().toString(), degreeMode);
                s = Tools.parseDouble(ets[16]);

                h = h + Tools.DEG(0, 0, h3) - Tools.DEG(0, h4, h5) - Tools.DEG(0, 0, h6);
                z = ((dn - dno) / 24.0 * t + dno) * s;
                pd = 90.0 - s * z;
                ss = (pd + h + lt) / 2.0;
                a = Tools.ACOS((Tools.SIN(z) - Tools.SIN(lt) * Tools.SIN(h)) / (Tools.COS(lt) * Tools.COS(h)));
                b = (Tools.INT(st / 6.0) * 6.0 + 3 - st) * Tools.SIN(lt);
                msg += "結果=" + Tools.Deg2DmsStr2(Tools.FRAC((b + a * Tools.SGN(12.0 - t1) - aa + 720.0) / 360.0) * 360.0);
            } else {
                Toast.makeText(this, "請確認已填入後半部必要的數據，才能計算結果",Toast.LENGTH_LONG).show();
            }
            mTvResult.setText(msg);
        } else {
            Toast.makeText(this, "請確認已填入前10項必要的數據，才能計算平均垂直角",Toast.LENGTH_LONG).show();
        }
    }
}
