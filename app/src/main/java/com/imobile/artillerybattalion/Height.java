package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import java.util.Arrays;
import java.util.Locale;

public class Height extends Activity {
    private final String TAG="MyLog";

    int ids[];
    EditText ets[];

    CheckBox cbHeight;
    Button btCalc, btReset, btDegreeMode;
    TextView mTvResult;

    int degreeMode = 0; // 0 auto, 1 Deg, 2 DMS, 3 Mil

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.height);
        cbHeight = (CheckBox)findViewById(R.id.cbHeight);
        cbHeight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cbHeight.setText("反覘");
                } else cbHeight.setText("直覘");
            }
        });

        ids = new int[] {
                R.id.etA, R.id.etD,R.id.etV,
                R.id.etE, R.id.etC
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }

        btDegreeMode = (Button)findViewById(R.id.btDegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

        btCalc = (Button)findViewById(R.id.btCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });

        btReset = (Button)findViewById(R.id.btReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        mTvResult = (TextView)findViewById(R.id.tvResult);
    }
    private boolean checkInput() {
        for (int i=0; i<ids.length-2; i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }

    private double inputDegree(String exp) {
        return Tools.auto2Deg(exp, degreeMode);
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }

    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }
        // # 90
        double a = Tools.parseDouble(ets[0]);
        double d = Tools.parseDouble(ets[1]);
        String ang = ets[2].getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double v = inputDegree(ang);
        double e;
        if (ets[3].getText().toString().equals("")) e = 0;
        else e = Tools.parseDouble(ets[3]);
        double c;
        if (ets[4].getText().toString().equals("")) c = 0;
        else c = Tools.parseDouble(ets[4]);
        double b = Tools.len2Height(a, d, v, e, c, cbHeight.isChecked()?-1:1)[0];

        // 有沒有注意到底下的訊息，竟然是 (y, x, h) 的順序
        mTvResult.setText(String.format(Locale.CHINESE, "標高 %.2f", b));
    }
}
