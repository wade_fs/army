/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

public class Intersection extends Activity {
    private final String TAG="MyLog";
    EditText etD, etA, etB;
    Button btCalc, btReset, btDegreeMode;
    TextView mTvResult;
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.intersection);

        etD = (EditText)findViewById(R.id.etD);
        etA = (EditText)findViewById(R.id.etA);
        etB = (EditText)findViewById(R.id.etB);

        mTvResult = (TextView)findViewById(R.id.tvResult);
        btCalc = (Button)findViewById(R.id.btCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        btDegreeMode = (Button)findViewById(R.id.btDegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

    }
    private boolean checkInput() {
        if (etD.getText().length() == 0 || etA.getText().length() == 0 || etB.getText().length() == 0) return false;
        return true;
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        etD.setText("");
        etA.setText("");
        etB.setText("");
        etD.requestFocus();
    }
    private double inputDegree(String exp) {
        return Tools.auto2Deg(exp, degreeMode);
    }

    private void calc() {
        double D, A, B;
        double m, s;
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }

        D = Tools.parseDouble(etD);
        A = inputDegree(etA.getText().toString());
        B = inputDegree(etB.getText().toString());

        double e = D / Tools.SIN(A-B);
        double l = e * Tools.SIN(B);
        double r = e * Tools.SIN(A);
        mTvResult.setText(String.format("求邊(左) %.2f\n求邊(右) %.2f", l, r));
    }
}
