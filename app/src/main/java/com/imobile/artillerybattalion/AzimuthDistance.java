/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

public class AzimuthDistance extends Activity {
    final private static String TAG="MyLog";
    int ids[];
    EditText ets[];

    Button btCalc, btReset, btDegreeMode;
    TextView mTvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.azimuth_distance);

        ids = new int[] {
                R.id.etBaseX, R.id.etBaseY, R.id.etTargetX, R.id.etTargetY,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }

        mTvResult = (TextView)findViewById(R.id.tvADResult);

        btCalc = (Button)findViewById(R.id.btADCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btADReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
    }

    private boolean checkInput() {
        for (int i=0; i<ids.length; i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }

    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
        }
        ets[0].requestFocus();
    }

    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }
        double X = Tools.parseDouble(ets[0]);
        double Y = Tools.parseDouble(ets[1]);
        double E = Tools.parseDouble(ets[2]);
        double F = Tools.parseDouble(ets[3]);
        double[] res = Tools.POLd(F-Y, E-X);
        mTvResult.setText(String.format("基點至求點距離 %.2f\n基點至求點方位角 %s",
                res[0], Tools.ang2Str(res[1])));
    }
}
