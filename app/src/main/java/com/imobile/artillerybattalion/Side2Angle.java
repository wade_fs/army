package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

public class Side2Angle extends Activity {
    private final String TAG="MyLog";
    
    int ids[];
    EditText ets[];

    Button btCalc, btDegreeMode, btReset;
    TextView mTvResult;
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.side2angle);
        ids = new int[]{
                R.id.etXL, R.id.etYL, R.id.etHL,
                R.id.etXR, R.id.etYR, R.id.etHR,
                R.id.etSBA, R.id.etEBA, R.id.etSCA, R.id.etECA,
                R.id.etS2AS, R.id.etS2AEL, R.id.etS2AER
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btS2ADegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });
        btCalc = (Button)findViewById(R.id.btS2ACalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btS2AReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvS2AResult);
    }

    private boolean checkInput() {
        for (int i=0; i<10; i++) {
            if (ets[i].getText().toString().trim().length() == 0) {
                return false;
            }
        }
        return true;
    }

    private double inputDegree(EditText exp) {
        return Tools.auto2Deg(exp.getText().toString(), degreeMode);
    }
    private double inputDegree(String ang) {
        return Tools.auto2Deg(ang, degreeMode);
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }

    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }
        boolean isNAN = false;
        /// 1. 先轉換角度, 從　度.分秒 ==> 度

        String ang = ets[7].getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double gba = inputDegree(ang);
        ang = ets[9].getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double gca = inputDegree(ang);
        Log.d("MyLog", "<7="+gba+", <9="+gca);

        /// 2. 設定其他值
        double bx = Tools.parseDouble(ets[0]);
        double by = Tools.parseDouble(ets[1]);
        double bh = Tools.parseDouble(ets[2]);
        double cx = Tools.parseDouble(ets[3]);
        double cy = Tools.parseDouble(ets[4]);
        double ch = Tools.parseDouble(ets[5]);
        double B1 = Tools.parseDouble(ets[6]);
        double C1 = Tools.parseDouble(ets[8]);
        double S  = Tools.parseDouble(ets[10]);
        double El = Tools.parseDouble(ets[11]);
        double Er = Tools.parseDouble(ets[12]);
        Log.d(TAG, "ets[10] = "+S+", ets[11] = "+El+", ets[12] = "+Er);

        /// 3. TODO: 開始計算，首先是 a = POLd(cy-by, cx-bx, x, y);
        double[] retPOL = Tools.POLd(cy-by, cx-bx);
        double a = retPOL[0];
        double y = retPOL[1];
        double bcz = Tools.FRAC(((y+360)/360))*360;

        /// 4. b1, c1 是斜距，b, c 是平距
        double C = C1 * Tools.SIN(gca);
        double B = B1 * Tools.SIN(gba);
        double e = (a + C + B) * 0.5;

        double q = 2*Tools.ACOS(Math.sqrt(e*(e-a)/(C*B)));
        double s = Tools.ASIN(B/a*Tools.SIN(q));
        double r = Tools.ASIN(C/a*Tools.SIN(q));
        isNAN = Double.isNaN(q) || Double.isNaN(s) || Double.isNaN(r);
        if (isNAN) {
            mTvResult.setText("輸入值造成無法計算，請仔細檢查後，再重新計算");
            return;
        }

        // 5. 修正
        double a2=0, B2=0, C2=0;
        if (q <= 90) {
            a2 = a*a;
            C2 = C*C;
            B2 = B*B;
            if (C2 > (a2 + B2)) {
                r = 180 - r;
            }
            if (B2 > (a2 + C2)) {
                s = 180 - s;
            }
        }

        double t = q+r+s;

        // 6. 前半的結果就出來了
        String msg = String.format("頂角 %s  左基內角 %s\n右基內角 %s 內角和 %s\n",
                Tools.Deg2DmsStr2(q), Tools.Deg2DmsStr2(r), Tools.Deg2DmsStr2(s), Tools.Deg2DmsStr2(t));

        // 7. 後半段需要先檢測 t 在合理範圍，也就是 180 度左右，誤差在 15" 以內
        if (Math.abs(t - 180) <= 4.16666666e-3) {
            double baz = bcz - r;
            baz = Tools.FRAC((baz+360.0)/360.0)*360.0;
            double caz = bcz - 180 + s;
            caz = Tools.FRAC((caz+360.0)/360.0)*360.0;

            double[] retREC = Tools.REC(C, caz);
            double ayc = retREC[0] + cy;
            double axc = retREC[1] + cx;
            double ahc = Tools.len2Height(ch, C1, gca, Er, S, +1)[0]; // c1 * Tools.COS(gca) + ch;

            ///    ayb = REC(c, baz, x, y)+by;
            ///    axb = y+bx;
            retREC = Tools.REC(B, baz);
            double ayb = retREC[0] + by;
            double axb = retREC[1] + bx;
            double ahb = Tools.len2Height(bh, B1, gba, El, S, +1)[0]; // b1 * Tools.COS(gba) + bh;

            msg += String.format("求點坐標 (%.2f, %.2f) 求點標高 %.2f", (axb+axc) * 0.5, (ayb+ayc) * 0.5, (ahb+ahc) * 0.5);
            mTvResult.setText(msg);
        }
    }
}
