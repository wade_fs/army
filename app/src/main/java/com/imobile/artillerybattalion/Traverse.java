package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import java.util.Locale;

public class Traverse extends Activity {
    private final String TAG="MyLog";

    int ids[];
    EditText ets[];
    TextView tvs[];

    Button mBtnCalc, mBtnReset, mBtDegreeMode;
    TextView mTvResult;

    int degreeMode = 0; // 0 auto, 1 Deg, 2 DMS, 3 Mil

    private boolean continued = false;
    private double x=0, y=0, h=0, p=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.traverse);
        ids = new int[] {
                R.id.etSCPX, R.id.etSCPY, R.id.etSCPH, R.id.etSCPP,
                R.id.etD, R.id.etA, R.id.etV,
                R.id.etE, R.id.etC
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        tvs = new TextView[3];
        tvs[0] = (TextView)findViewById(R.id.tv1);
        tvs[1] = (TextView)findViewById(R.id.tv2);
        tvs[2] = (TextView)findViewById(R.id.tv3);

        mBtDegreeMode = (Button)findViewById(R.id.btDegreeMode);
        mBtDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: mBtDegreeMode.setText("<自動"); break;
                    case 1: mBtDegreeMode.setText("度小數");break;
                    case 2: mBtDegreeMode.setText("度分秒");break;
                    case 3: mBtDegreeMode.setText("密位"); break;
                }
            }
        });

        mBtnCalc = (Button)findViewById(R.id.btCalc);
        mBtnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
                for (int i=0; i<4; i++) ets[i].setEnabled(false);
//                ets[4].requestFocus();
            }
        });

        mBtnReset = (Button)findViewById(R.id.btReset);
        mBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        mTvResult = (TextView)findViewById(R.id.tvResult);
    }
    private boolean checkInput() {
        for (int i=0; i<ids.length-2; i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }

    private double inputDegree(EditText et) {
        return Tools.auto2Deg(et.getText().toString(), 0);
    }
    private double inputDegree(String ang) {
        return Tools.auto2Deg(ang, 0);
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
        tvs[0].setText("已知點橫坐標");
        tvs[1].setText("已知點縱坐標");
        tvs[2].setText("已知點標高");
        mTvResult.setText("");

        continued = false;
        x = y = h = p = 0;
    }
    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!continued) { // #40
            x = Tools.parseDouble(ets[0]);
            y = Tools.parseDouble(ets[1]);
            h = Tools.parseDouble(ets[2]);

            p = inputDegree(ets[3]);
            continued = true;
        }
        // # 90
        double d = Tools.parseDouble(ets[4]);
        double a = inputDegree(ets[5]);

        String ang = ets[6].getText().toString();
        if (ang.trim().equals("0")) ang = "1600";

        double v = inputDegree(ang);
        double[] res = Tools.traverse3(x, y, h, p, d, a, v);
        x = res[0]; y = res[1]; h = res[2]; a = res[3]; p = res[4]; v = res[5];

        double e = Tools.parseDouble(ets[7]);
        double c = Tools.parseDouble(ets[8]);

        h = h + e - c; // 已經在前面 traverse3 中調整橢球高了

        if (continued) {
            ets[0].setText(String.format("%.2f",x));
            ets[1].setText(String.format("%.2f",y));
            ets[2].setText(String.format("%.2f",h));
            ets[3].setText(String.format("%.2f",p));
            for (int i=4; i<=8; i++) ets[i].setText("");
            tvs[0].setText("前一點橫坐標");
            tvs[1].setText("前一點縱坐標");
            tvs[2].setText("前一點標高");
            ets[4].requestFocus();
        }
        // 有沒有注意到底下的訊息，竟然是 (y, x, h) 的順序
        mTvResult.setText(String.format(Locale.CHINESE, "求點坐標=%.2f,%.2f\n求點標高=%.2f\n方位角=%s",
                x, y, h, Tools.ang2Str(a)));
    }
}
