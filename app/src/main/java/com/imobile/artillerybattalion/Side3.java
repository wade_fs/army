package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

public class Side3 extends Activity {
    private final String TAG="MyLog";
    
    int ids[];
    EditText ets[];

    Button btCalc, btDegreeMode, btReset;
    TextView mTvResult;
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.side3);
        ids = new int[]{
                R.id.etS3L, R.id.etS3R, R.id.etS3D,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btS3DegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });
        btCalc = (Button)findViewById(R.id.btS3Calc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btS3Reset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvS3Result);
    }

    private boolean checkInput() {
        for (int i=0; i<ids.length; i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }

    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }

    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }
        double a = Tools.parseDouble(ets[0].getText().toString());
        double b = Tools.parseDouble(ets[1].getText().toString());
        double c = Tools.parseDouble(ets[2].getText().toString());

        if (a+b <= c || b+c<=a || c+a<=b || a <= 0 || b <= 0 || c<=0) {
            Toast.makeText(this, "請檢查三邊的值是否合理!!", Toast.LENGTH_SHORT).show();
            return;
        }
        double aa = a*a;
        double bb = b*b;
        double cc = c*c;
        double ab = a*b;
        double bc = b*c;
        double ca = c*a;

        double s = Tools.ACOS((aa+bb-cc)/(2*ab));
        double r = Tools.ACOS((cc+aa-bb)/(2*ca));
        double q = Tools.ACOS((bb+cc-aa)/(2*bc));

        mTvResult.setText(String.format("內角Q = %s, 內角R = %s, 內角S = %s", Tools.Deg2DmsStr2(q), Tools.Deg2DmsStr2(r), Tools.Deg2DmsStr2(s)));
    }
}
