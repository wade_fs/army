package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;
// 方格統一計算
public class CoordinateAdj extends Activity {
    private final String TAG="MyLog";
    
    int ids[];
    EditText ets[];
    
    Button btCalc, btDegreeMode, btReset;
    TextView mTvResult;
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.coordinate_adj);
        ids = new int[]{
                R.id.etASSX, R.id.etASSY, R.id.etASSH, R.id.etASSP,
                R.id.etSCPX, R.id.etSCPY, R.id.etSCPH, R.id.etSCPP,
                R.id.etX, R.id.etY, R.id.etH,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btCADegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

        btCalc = (Button)findViewById(R.id.btCACalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btCAReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvCAResult);
    }

    private boolean checkInput() {
        for (int i=0; i<ids.length; i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }

    private double inputDegree(EditText et) {
        if (et == null) return 0;
        return Tools.auto2Deg(et.getText().toString(), degreeMode);
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }
    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }

        /// 1. 先轉換角度, 從　度.分秒 ==> 度
        ///     EditText mEtASSX, mEtASSY, mEtASSH, mEtASSP, mEtSCPX, mEtSCPY, mEtSCPH, mEtSCPP;
        ///     EditText mEtX, mEtY, mEtH;

        double q = inputDegree(ets[3]);
        double p = inputDegree(ets[7]);
        p = p-q;

        /// 2. 設定其他值
        double l = Tools.parseDouble(ets[0]);
        double m = Tools.parseDouble(ets[1]);
        double n = Tools.parseDouble(ets[2]);
        double u = Tools.parseDouble(ets[4]);
        double v = Tools.parseDouble(ets[5]);
        double w = Tools.parseDouble(ets[6]);
        double x = Tools.parseDouble(ets[8]);
        double y = Tools.parseDouble(ets[9]);
        double h = Tools.parseDouble(ets[10]);

        double[] retPOL = Tools.POLd(y-m, x-l);
        double d = retPOL[0];
        y = retPOL[1];
        double a = y+p;

        /// 4. TODO C:
        ///    x = REC(d, a, x, y)+v;  <-- x 為什麼加的是 v?
        ///    y = y+u;                <-- y 為什麼加的是 u?
        /// 結論是 REC(d, a, x, y) ==> x = d*cos(a), y=d*sin(a)
        double[] retREC = Tools.REC(d, a);
        x = v + retREC[0];
        y = retREC[1] + u;

        h = w -n + h;

        // 有沒有注意到底下的訊息，竟然是 (y, x, h) 的順序
        mTvResult.setText(String.format("正確求點坐標、標高(x,y,h) = (%.2f, %.2f, %.2f)", y, x, h));
    }
}
