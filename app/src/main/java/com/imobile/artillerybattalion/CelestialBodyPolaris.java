package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

public class CelestialBodyPolaris extends Activity {
    public static final String TAG = "MyLog";
    
    Button btCalc, btReset, btDegreeMode, btSwitch;
    TextView mTvResult;

    int ids[];
    EditText ets[];

    int degreeMode=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.celestial_body_polaris);
        ids = new int[] {
                R.id.ett1, R.id.ett2, R.id.ett3,
                R.id.eta1, R.id.eta2, R.id.eta0,
                R.id.etst, R.id.etlt,
                R.id.etdno, R.id.etdn,
                R.id.etb0, R.id.etb1, R.id.etb2,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }

        btSwitch = (Button)findViewById(R.id.btCBPSwitch);
        btSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CelestialBodySunHeight.class);
                startActivity(intent);
            }
        });
        btDegreeMode = (Button)findViewById(R.id.btCBPDegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

        btCalc = (Button)findViewById(R.id.btCBPCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btCBPReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvCBPResult);
    }

    private boolean checkInput(int part) {
        if (part == 0) {
            for (int i = 0; i < 10; i++) {
                if (ets[i].getText().length() == 0) return false;
            }
        } else if (part == 1) {
            for (int i = 10; i < ids.length; i++) {
                if (ets[i].getText().length() == 0) return false;
            }
        }
        return true;
    }

    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }
    private String Deg2DmsStr2(double a) {
        double[] res = Tools.Deg2Dms3(a);
        return String.format("%s%d時%02d分%.3f秒", (res[0]==-1?"-":""), (int)res[1], (int)res[2], res[3]);
    }
    private void calc() {
        if (checkInput(0)) {
            double t1 = Tools.auto2Deg(ets[0].getText().toString(), degreeMode);
            double t2 = Tools.auto2Deg(ets[1].getText().toString(), degreeMode);
            double t3 = Tools.auto2Deg(ets[2].getText().toString(), degreeMode);
            double a1 = Tools.auto2Deg(ets[3].getText().toString(), degreeMode);
            double a2 = Tools.auto2Deg(ets[4].getText().toString(), degreeMode);
            double a0 = Tools.auto2Deg(ets[5].getText().toString(), degreeMode);
            double st = Tools.auto2Deg(ets[6].getText().toString(), degreeMode);
            double lt = Tools.auto2Deg(ets[7].getText().toString(), degreeMode);
            double dno = Tools.auto2Deg(ets[8].getText().toString(), degreeMode);
            double dn = Tools.auto2Deg(ets[9].getText().toString(), degreeMode);

            double t = (t1 + t2) / 2 + t3 - 8.0;
            double aa = (a1 + a2 - a0) / 2 + Tools.FIX(a1 / 180.0) * 180.0;
            double tt = t + dno + t / 24.0 * (dn - dno) + st / 360.0 * 24;
            mTvResult.setText("當地恒星時觀測時間=" + Deg2DmsStr2(tt - Tools.FIX(tt / 24.0) * 24) + "\n");
            if (checkInput(1)) {
                double b0 = Tools.parseDouble(ets[10]);
                double b1 = Tools.parseDouble(ets[11]);
                double b2 = Tools.parseDouble(ets[12]);

                double a = Tools.DEG(0, b0 + b1 + b2) / Tools.COS(lt);
                a = (1 - Tools.SGN(a)) * 180.0 + a;
                double b = (Tools.INT(st / 6.0) * 6.0 + 3 - st) * Tools.SIN(lt);
                // 235.526085
                String msg = mTvResult.getText() + "結果=" + Tools.Deg2DmsStr2(Tools.FRAC((b + a - aa + 720.0) / 360.0) * 360.0);
                mTvResult.setText(msg);
            }
        } else Toast.makeText(this, "請確認已填入上方各欄位數值，再按計算", Toast.LENGTH_LONG).show();
    }
    @Override protected void onStop() {
        super.onStop();
        finish();
    }
}
