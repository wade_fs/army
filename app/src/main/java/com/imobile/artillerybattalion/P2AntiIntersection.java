package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import org.w3c.dom.Text;

public class P2AntiIntersection extends Activity {
    private final String TAG="MyLog";

    Button btReset, btDegreeMode, btCalc;
    TextView mTvResult;

    int ids[];
    EditText ets[];
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.p2_anti_intersection);

        ids = new int[]{
                R.id.etP2XA, R.id.etP2YA,
                R.id.etP2XB, R.id.etP2YB,
                R.id.etP2A1, R.id.etP2A2,
                R.id.etP2A3, R.id.etP2A4,
                R.id.etP2HA, R.id.etP2HB,
                R.id.etP2PA, R.id.etP2QA,
                R.id.etP2PB, R.id.etP2QB,
                R.id.etP2EP, R.id.etP2EQ,
                R.id.etP2CA, R.id.etP2CB,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btP2DegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });
        btCalc = (Button)findViewById(R.id.btP2Calc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });

        btReset = (Button)findViewById(R.id.btResetP2);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvResultP2);
    }

    private boolean checkInput(int s) {
        for (int i=0; i<((s==1)?8:14); i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }
    public void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
        }
        ets[0].requestFocus();
    }
    private double inputDegree(String exp) {
        double ret = Tools.auto2Deg(exp, degreeMode);
        return ret;
    }
    public void calc() {
        try {
            if (!checkInput(1)) {
                Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
                return;
            }

            double[] X = new double[7];
            double[] Y = new double[7];
            double[] A = new double[7];
            double[] H = new double[7];
            double[] V = new double[5];

            A[1] = inputDegree(ets[4].getText().toString());
            A[2] = inputDegree(ets[5].getText().toString());
            A[3] = inputDegree(ets[6].getText().toString());
            A[4] = inputDegree(ets[7].getText().toString());
            X[1] = Tools.parseDouble(ets[0]);
            Y[1] = Tools.parseDouble(ets[1]);
            X[2] = Tools.parseDouble(ets[2]);
            Y[2] = Tools.parseDouble(ets[3]);
            
            A[5] = 180.0 - (A[4] + A[3] + A[2]);
            A[6] = 180.0 - (A[1] + A[2] + A[3]);

            double k = Tools.ATAN(Tools.SIN(A[2]) * Tools.SIN(A[4]) * Tools.SIN(A[6])/
                        (Tools.SIN(A[1]) * Tools.SIN(A[3]) * Tools.SIN(A[5])));
            //110
            double f = (A[3] + A[2]) / 2;
            double g = Tools.ATAN(Tools.TAN(k-45) * Tools.TAN(f));
            double a = f+g;
            double b = f-g;
            //150
            double[] dPol = Tools.POLd(Y[2] - Y[1], X[2] - X[1]);
            double d = dPol[0];
            double y = dPol[1];
            y = y - 360.0*Tools.INT(y/360.0);
            //170
            double[] P = new double[5];
            P[1] = y+a+A[6];     P[1] = P[1] - 360.0 * Tools.INT(P[1]/360.0);
            P[2] = y+a;          P[2] = P[2] - 360.0 * Tools.INT(P[2]/360.0);
            P[3] = y+180-b;      P[3] = P[3] - 360.0 * Tools.INT(P[3]/360.0);
            P[4] = y+180-b-A[5]; P[4] = P[4] - 360.0 * Tools.INT(P[4]/360.0);
            //210
            double[] D = new double[5];
            D[1] = d / Tools.SIN(A[1]) * Tools.SIN(b);
            D[2] = d / Tools.SIN(A[4]) * Tools.SIN(A[5]+b);
            D[3] = d / Tools.SIN(A[1]) * Tools.SIN(A[6]+a);
            D[4] = d / Tools.SIN(A[4]) * Tools.SIN(a);
            //250
            X[3] = X[1] + D[1] * Tools.SIN(P[1]);
            Y[3] = Y[1] + D[1] * Tools.COS(P[1]);
            X[5] = X[1] + D[2] * Tools.SIN(P[2]);
            Y[5] = Y[1] + D[2] * Tools.COS(P[2]);
            X[4] = X[2] + D[3] * Tools.SIN(P[3]);
            Y[4] = Y[2] + D[3] * Tools.COS(P[3]);
            X[6] = X[2] + D[4] * Tools.SIN(P[4]);
            Y[6] = Y[2] + D[4] * Tools.COS(P[4]);

            if (checkInput(2)) {
                String ang;
                ang = ets[10].getText().toString();
                if (ang.trim().equals("0")) ang = "1600";
                V[1] = inputDegree(ang);

                ang = ets[11].getText().toString();
                if (ang.trim().equals("0")) ang = "1600";
                V[2] = inputDegree(ang);

                ang = ets[12].getText().toString();
                if (ang.trim().equals("0")) ang = "1600";
                V[3] = inputDegree(ang);

                ang = ets[13].getText().toString();
                if (ang.trim().equals("0")) ang = "1600";
                V[4] = inputDegree(ang);

                f = Tools.parseDouble(ets[14]);
                g = Tools.parseDouble(ets[15]);
                double u = Tools.parseDouble(ets[16]);
                double z = Tools.parseDouble(ets[17]);

                //380
                H[1] = Tools.parseDouble(ets[8]);
                H[2] = Tools.parseDouble(ets[9]);
                H[3] = Tools.len2Height(H[1], D[1], V[1], f, u, -1)[0]; // H[1] + u - f - D[1] * Tools.TAN(V[1]) - W * D[1] * D[1];
                H[5] = Tools.len2Height(H[1], D[2], V[2], g, u, -1)[0]; // H[1] + u - g - D[2] * Tools.TAN(V[2]) - W * D[2] * D[2];
                H[4] = Tools.len2Height(H[2], D[3], V[3], f, z, -1)[0]; // H[2] + z - f - D[3] * Tools.TAN(V[3]) - W * D[3] * D[3];
                H[6] = Tools.len2Height(H[2], D[4], V[4], g, z, -1)[0]; // H[2] + z - g - D[4] * Tools.TAN(V[4]) - W * D[4] * D[4];
                mTvResult.setText(String.format(
                        "P點橫坐標 %.2f, P點縱坐標 %.2f\n" +
                                "Q點橫坐標 %.2f, Q點縱坐標 %.2f\n" +
                                "P點標高 %.2f, Q點標高 %.2f\n",
                        ((X[3] + X[4]) / 2), ((Y[3] + Y[4]) / 2),
                        ((X[5] + X[6]) / 2), ((Y[5] + Y[6]) / 2),
                        ((H[3] + H[4]) / 2), ((H[5] + H[6]) / 2)
                ));
            } else {
                mTvResult.setText(String.format(
                        "P點橫坐標 %.2f, P點縱坐標 %.2f\n"+
                                "Q點橫坐標 %.2f, Q點縱坐標 %.2f\n",
                        ((X[3]+X[4])/2), ((Y[3]+Y[4])/2),
                        ((X[5]+X[6])/2), ((Y[5]+Y[6])/2)
                ));
            }
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, "請確認是不是有輸入值，再按計算", Toast.LENGTH_SHORT).show();
        }
    }
}
