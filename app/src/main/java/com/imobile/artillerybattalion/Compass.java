package com.imobile.artillerybattalion;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GnssMeasurementsEvent;
import android.location.GnssNavigationMessage;
import android.location.GnssStatus;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.OnNmeaMessageListener;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Surface;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.CPDB;
import com.imobile.libs.Proj;
import com.imobile.libs.Tools;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

class Compass implements SensorEventListener, LocationListener {

    private static final String TAG = "MyLog";

    private CompassActivity sInstance;
    private SensorManager mSensorManager;
    private Sensor gSensor;
    private Sensor mSensor;
    private Sensor oSensor;
    private Sensor rSensor;
    private float[] smoothed = new float[3];
    private float[] mGravity = new float[3];
    private float[] mGeomagnetic = new float[3];
    private float[] mOrientation = new float[3];
    private float[] mRotation = new float[3];
    private float azimuth = 0f, tazimuth=0f;
    private float currectAzimuth = 0, fixAzimuth = 0;
    private int count = 0;
    private boolean pause;

    // compass arrow to rotate
    ImageView arrowView = null;
    ImageView compassView = null;
    TextView mTvCompass, mTvGps, mTvControllPoint;

    // GPS
    private static final int SECONDS_TO_MILLISECONDS = 1000;
    private static float[] mRotationMatrix = new float[16];
    private static float[] mRemappedMatrix = new float[16];
    private static float[] mValues = new float[3];
    private static float[] mTruncatedRotationVector = new float[4];
    private static boolean mTruncateVector = false;
    private boolean mStarted;
    private boolean mFaceTrueNorth;
    private LocationManager mLocationManager;
    private LocationProvider mProvider;
    private GpsStatus mLegacyStatus;
    private GpsStatus.Listener mLegacyStatusListener;
    private GpsStatus.NmeaListener mLegacyNmeaListener;

    private GnssStatus mGnssStatus;
    private GnssStatus.Callback mGnssStatusListener;
    private GnssMeasurementsEvent.Callback mGnssMeasurementsListener; // For SNRs
    private OnNmeaMessageListener mOnNmeaMessageListener;
    private GnssNavigationMessage.Callback mGnssNavMessageListener;

    private Location mLastLocation;
    private GeomagneticField mGeomagneticField;
    private long minTime = 1 * SECONDS_TO_MILLISECONDS; // Min Time between location updates, in milliseconds
    private float minDistance = 0; // Min Distance between location updates, in meters

    private static final int PRN_COLUMN = 0;
    private static final int FLAG_IMAGE_COLUMN = 1;
    private static final int SNR_COLUMN = 2;
    private static final int ELEVATION_COLUMN = 3;
    private static final int AZIMUTH_COLUMN = 4;
    private static final int FLAGS_COLUMN = 5;
    private static final int COLUMN_COUNT = 6;
    private static final String EMPTY_LAT_LONG = "             ";
    SimpleDateFormat mDateFormat = new SimpleDateFormat("hh:mm:ss.SS a");
    private boolean mNavigating;

    private Proj mProj;
    private CPDB cpdb;

    private void warn(String msg, int level) {
        if (level < 2) Log.d("MyLog", msg);
        if (level != 1) Toast.makeText(sInstance, msg, Toast.LENGTH_LONG).show();
    }

    Compass(Context context) {
        sInstance = (CompassActivity) context;
        mSensorManager = (SensorManager) sInstance.getSystemService(Context.SENSOR_SERVICE);
        mLocationManager = (LocationManager) sInstance.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            warn("請檢查本應用程式是否授權使用『位置』及『儲存』", 0);
            sInstance.finish();
            return;
        }
        mProvider = mLocationManager.getProvider(LocationManager.GPS_PROVIDER);
        if (mProvider == null) {
            warn("未支援 GPS", 0);
            sInstance.finish();
            return;
        }
        minTime = (long) (SECONDS_TO_MILLISECONDS);
        minDistance = 0;
        mTvGps = (TextView)sInstance.findViewById(R.id.tvGPS);
        mTvControllPoint = (TextView)sInstance.findViewById(R.id.tvControlPoint);

        gSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        oSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        rSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mProj = new Proj();
        cpdb = new CPDB(context);
    }

    public void start() {
        mSensorManager.registerListener(this, gSensor, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
//        mSensorManager.registerListener(this, oSensor, SensorManager.SENSOR_DELAY_GAME);
//        mSensorManager.registerListener(this, rSensor, SensorManager.SENSOR_DELAY_GAME);
//        addOrientationSensorListener();

        if (ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            promptEnableGps();
        }
        mLocationManager.requestLocationUpdates(mProvider.getName(), minTime, minDistance, this);
        mFaceTrueNorth = true;
        gpsStart();
    }

    void stop() {
        if (gSensor != null) mSensorManager.unregisterListener(this, gSensor);
        if (mSensor != null) mSensorManager.unregisterListener(this, mSensor);
        if (oSensor != null) mSensorManager.unregisterListener(this, oSensor);
        if (rSensor != null) mSensorManager.unregisterListener(this, rSensor);
        gpsStop();
    }

    void setPause(boolean p) { pause = p; count = 0; }

    private void adjustArrow() {
        if (compassView == null) {
            return;
        }
        compassView.clearAnimation();
        azimuth = (-mOrientation[0]+90+360)%360;
        if (mFaceTrueNorth && mGeomagneticField != null) {
            tazimuth = (azimuth+mGeomagneticField.getDeclination()+360)%360;
        } else tazimuth = azimuth;

        Animation an = new RotateAnimation(-currectAzimuth, -tazimuth,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        an.setDuration(200);
        an.setRepeatCount(0);
        an.setFillAfter(true);
        compassView.startAnimation(an);

        currectAzimuth = tazimuth;

        if (!pause) {
            if (++count%100 == 1) {
                mTvCompass.setTextColor(Color.BLUE);
                showCompassText(currectAzimuth, false);
            }
            fixAzimuth = tazimuth;

            if (count < 0) {
                count = 0;
            }
        }
        else {
            mTvCompass.setTextColor(Color.RED);
            showCompassText(fixAzimuth, true);
        }
    }

    private void showCompassText(float ta, boolean pause) {
        if (mFaceTrueNorth && mGeomagneticField != null) {
            double a = (ta+mGeomagneticField.getDeclination()+360)%360;
            mTvCompass.setText(String.format(Locale.CHINESE, "正北方位角 %s\n  %.3f度(%s)\n  %.2f mil(密位)\n磁方位角\n  %.3f度(%s)\n  %.2f mil(密位)\n",
                    (pause?"<<< 暫停更新 >>>":""),
                    ta,
                    Tools.Deg2DmsStr(ta),
                    Tools.Deg2Mil(ta),
                    a,
                    Tools.Deg2DmsStr(a),
                    Tools.Deg2Mil(a)
            ));
        } else {
            mTvCompass.setText(String.format(Locale.CHINESE, "磁方位角 %s\n  %.3f度(%s)\n  %.2f mil(密位)\n",
                    (pause?"<<< 暫停更新 >>>":""),
                    ta,
                    Tools.Deg2DmsStr(ta),
                    Tools.Deg2Mil(ta)
            ));
        }
    }

    boolean hasMAG = false;
    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.97f; // 緩慢修正值

        synchronized (this) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mGravity[0] = alpha * mGravity[0] + (1 - alpha) * event.values[0];
                mGravity[1] = alpha * mGravity[1] + (1 - alpha) * event.values[1];
                mGravity[2] = alpha * mGravity[2] + (1 - alpha) * event.values[2];
            }
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha) * event.values[0];
                mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha) * event.values[1];
                mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha) * event.values[2];
            }
            float R[] = new float[9];
            float I[] = new float[9];
            hasMAG = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (hasMAG) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                mOrientation[0] = (float)Math.toDegrees(orientation[0]);
                adjustArrow();
            }
        }
    }
    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) return input;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + 0.2f * (input[i] - output[i]);
        }
        return output;
    }
    // GPS

    @Override public void onAccuracyChanged(Sensor sensor, int accuracy) {}
    public void onStatusChanged(String provider, int status, Bundle extras) {}
    public void onProviderEnabled(String provider) {}
    public void onProviderDisabled(String provider) {}

    public void onOrientationChanged(double orientation, double tilt) {
        warn("onOrientationChanged("+orientation+","+tilt+")", 1);
    }
    public static boolean isRotationVectorSensorSupported(Context context) {
        SensorManager sensorManager = (SensorManager) context
                .getSystemService(Context.SENSOR_SERVICE);
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD &&
                sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null;
    }

    private void addOrientationSensorListener() {
        if (isRotationVectorSensorSupported(sInstance)) {
            // Use the modern rotation vector sensors
            Sensor vectorSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            mSensorManager.registerListener(this, vectorSensor, 16000); // ~60hz
        } else {
            // Use the legacy orientation sensors
            Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
            if (sensor != null) {
                mSensorManager.registerListener(this, sensor,
                        SensorManager.SENSOR_DELAY_GAME);
            }
        }
    }
    private void promptEnableGps() {
        new AlertDialog.Builder(sInstance)
                .setMessage("啟動 GPS")
                .setPositiveButton("啟動 GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                sInstance.startActivity(intent);
                            }
                        }
                )
                .setNegativeButton("關閉",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void removeStatusListener() {
        if (mLocationManager != null && mLegacyStatusListener != null) {
            mLocationManager.removeGpsStatusListener(mLegacyStatusListener);
        }
    }

    private double lastX = 0, lastY = 0;

    public static String cpName(int t) {
        switch (t) {
            case 1: return "一等連續站";
            case 2: return "一等控制點";
            case 3: return "二等控制點";
            case 4: return "三等控制點";
            case 5: return "一等水準點";
            case 6: return "金門";
            case 7: return "馬祖";
            case 8: return "澎湖";
            case 9: return "永康";
            case 10: return "歸仁";
            case 11: return "關廟";
            default:return "";
        }
    }
    public void onLocationChanged(Location location) {
        mLastLocation = location;

        mGeomagneticField = new GeomagneticField((float) location.getLatitude(),
                (float) location.getLongitude(), (float) location.getAltitude(),
                location.getTime());
        double[] res = mProj.LL2UTM(location.getLongitude(), location.getLatitude(), 0);
        double[] res2 = mProj.LL2TM2(location.getLongitude(), location.getLatitude());
        mTvGps.setText(String.format(Locale.CHINESE, "==GPS資訊%s==\n經度=%s\n緯度=%s\n橢球高=%.2f公尺\n速度=%.2f公尺/秒\n航向=%s\nUTM6 E%.2f, N%.2f\nTM2    E%.2f, N%.2f",
                location.hasAccuracy()?String.format("(%.1f公尺)",location.getAccuracy()):"",
                Tools.Deg2DmsStr2(location.getLongitude()),
                Tools.Deg2DmsStr2(location.getLatitude()),
                location.getAltitude(),
                location.getSpeed(),
                Tools.ang2Str(location.getBearing()),
                res[0], res[1],
                res2[0], res2[1]));

        res = mProj.LL2TM2(location.getLongitude(), location.getLatitude());
        double dx = res[0] - lastX;
        double dy = res[1]  - lastY;
        if (Math.sqrt(dx*dx + dy*dy) > 1) {
            lastX = res[0];
            lastY = res[1];
            List<CPDB.CP> cps = cpdb.getCp(lastX, lastY, 0);
            if (cps.size() > 0) {
                Collections.sort(cps, new Comparator<CPDB.CP>() {
                    @Override
                    public int compare(CPDB.CP o1, CPDB.CP o2) {
                        double d = (Math.pow(o1.x - lastX, 2) + Math.pow(o1.y - lastY, 2)) -
                                (Math.pow(o2.x - lastX, 2) + Math.pow(o2.y - lastY, 2));
                        if (d > 0) return 1;
                        else if (d == 0) return 0;
                        else return -1;
                    }
                });
                String cpMsg = "== 鄰近控制點 ==\n";
                for (CPDB.CP cp : cps) {
                    double dx1 = cp.x - res[0], dy1 = cp.y-res[1];
                    double[] resCP = Tools.POLd(dy1, dx1);
                    cpMsg += String.format(Locale.CHINESE, "\n[%s]%s\n@%d(%s)\n%.0fE,%.0fN\n距離=%.2f公尺\n方位=%s\n",
                            cp.number, (cp.name.length()>0?cp.name:""),
                            cp.t, cpName(cp.t),
                            cp.x, cp.y,
                            resCP[0], Tools.Deg2DmsStr2(resCP[1])
                    );
                }
//                Log.d(TAG, cpMsg);
                mTvControllPoint.setText(cpMsg);
            }
        }
    }
    private void setStarted(boolean navigating) {
        if (navigating != mNavigating) {
            mNavigating = navigating;
        }
    }

    public void onGpsStarted() {
        setStarted(true);
    }
    public void onGpsStopped() {
        setStarted(false);
    }

    @Deprecated
    private void updateLegacyStatus(GpsStatus status) {
        setStarted(true);
    }

    @Deprecated
    public void onGpsStatusChanged(int event, GpsStatus status) {
        warn("onGpsStatusChanged("+event+","+status.toString()+")", 0);
        switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                setStarted(true);
                break;

            case GpsStatus.GPS_EVENT_STOPPED:
                setStarted(false);
                break;

            case GpsStatus.GPS_EVENT_FIRST_FIX:
                warn("Time to First Fix", 0);
                break;

            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                updateLegacyStatus(status);
                break;
        }
    }

    private void getRotationMatrixFromTruncatedVector(float[] vector) {
        System.arraycopy(vector, 0, mTruncatedRotationVector, 0, 4);
        SensorManager.getRotationMatrixFromVector(mRotationMatrix, mTruncatedRotationVector);
    }

    public synchronized void gpsStart() {
        if (!mStarted) {
            warn("gpsStart()", 1);
            if (ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                return;
            }
            mLocationManager.requestLocationUpdates(mProvider.getName(), minTime, minDistance, this);
            mStarted = true;
        }
    }
    public synchronized void gpsStop() {
        if (mStarted) {
            warn("gpsStop()", 1);
            mLocationManager.removeUpdates(this);
            mStarted = false;
        }
        removeStatusListener();
    }
}
