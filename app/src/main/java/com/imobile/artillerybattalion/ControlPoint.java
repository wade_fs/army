package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.CPDB;
import com.imobile.libs.Proj;
import com.imobile.libs.Tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

public class ControlPoint extends Activity
{
    private final String TAG = "MyLog";
    private TextView mTvResult;
    private ArrayAdapter<String> aaCP;
    private Spinner spCP;
    private EditText etQuery;
    private String[] html;
    private Proj mProj;
    private CPDB cpdb;

    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        setContentView(R.layout.controlpoint);
        this.mTvResult = ((TextView)findViewById(R.id.tvResult));
        this.mTvResult.setVerticalScrollBarEnabled(true);
        this.mTvResult.setMovementMethod(ScrollingMovementMethod.getInstance());
        mProj = new Proj();
        cpdb = new CPDB(this);
        spCP = (Spinner)findViewById(R.id.spCP);
        aaCP = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.stControlPoint));
        aaCP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCP.setAdapter(aaCP);
        spCP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mTvResult.setText("====== "+spCP.getSelectedItem().toString()+" ======\n"+html[position-1]);
                    etQuery.setText("");
                } else {
                    mTvResult.setText("語法如下:\n" +
                            "#點號\n" +
                            "TM2 橫坐標,縱坐標,距離  => TM2+橫坐標+縱坐標+距離\n" +
                            "WGS84 經度,緯度,距離   => WGS84+經度+緯度+距離\n" +
                            "距離的單位為(公尺), 若距離為0 表示查詢最近之控制點\n" +
                            "經緯度表示法主要有\n" +
                            "\tDD°MM'SS.SS\"\n" +
                            "\tDD.MMSS\n" +
                            "\t度小數 ##.#######\n" +
                            "例如: #8118 或 #e006 或 tm2 306241,2635805,1000 或\n" +
                            "\twgs 121°33'47.274&quot;,23.5044655,2000\n\n" +
                            "\t請不要使用 UTM6 坐標\n" +
                            "\t英文大寫小寫視為相同\n" +
                            "\t為了輸入方便，空白或逗號可以用+代替");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                spCP.setSelection(0);
            }
        });
        html = new String[] {
                loadHtml("GPS-1.html"),
                loadHtml("GPS-2.html"),
                loadHtml("GPS-3.html"),
                loadHtml("GPS-4.html"),
                loadHtml("GPS-5.html"),
                loadHtml("GPS-6.html"),
                loadHtml("GPS-7.html"),
                loadHtml("GPS-8.html"),
                loadHtml("GPS-9.html"),
                loadHtml("GPS-10.html"),
                loadHtml("GPS-11.html")
        };
        etQuery = (EditText)findViewById(R.id.etQuery);
        etQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double x=0, y=0, l=0;
                String number, query = (""+s).toUpperCase().replaceAll("[+]", " ").replaceAll(",", " ").replaceAll("  "," ");
                if (query.length() == 0) return;
                if (query.charAt(0) == '#') {
                    query = query.substring(1).trim();
                    if (query.length() < 3) return;
                    List<CPDB.CP> cps = cpdb.getCpByNumber(query);
                    String msg = "====== 查詢點號 '"+query+"' ======\n";
                    for (CPDB.CP cp:cps) {
                        msg += String.format(Locale.CHINESE, "[%s]%s@%d(%s)\n(X=%.2f, Y=%.2f, 高程 %.2f公尺)\n%s\n\n",
                                cp.number, (cp.name.length() > 0 ? cp.name : ""),
                                cp.t, Compass.cpName(cp.t),
                                cp.x, cp.y, cp.h,
                                cp.info);
                    }
                    mTvResult.setText(msg);
                } else {
                    double[] q = parseQuery(query);
                    if (q.length != 3 || q[0] == 0 || q[1] == 0 || q[2] == 0) return;
                    if (query.contains("W")) {
                        double[] res = mProj.LL2TM2(q[0], q[1]);
                        x = res[0];
                        y = res[1];
                    } else {
                        x = q[0];
                        y = q[1];
                    }
                    l = q[2];
//                Log.d(TAG, "==> x = "+x+", y = "+y+", l="+l);
                    List<CPDB.CP> cps = cpdb.getCp(x, y, l);
                    if (cps.size() > 0) {
                        String cpMsg = "====== 查詢控制點 '" + query + "' ======\n";
                        for (CPDB.CP cp : cps) {
                            cpMsg += String.format(Locale.CHINESE, "%s(X=%.2f, Y=%.2f, 高程 %.2f公尺)@%d%s\n\t%s\n\n",
                                    cp.number, cp.x, cp.y, cp.h,
                                    cp.t, (cp.name.length() > 0 ? "(" + cp.name + ")" : ""),
                                    cp.info);
                        }
                        mTvResult.setText(cpMsg);
                    } else {
                        mTvResult.setText("查無控制點");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private double[] parseQuery(String query) {
        double x=0, y=0, l=0;
        int i;
        char c;
        if (query == null || query.length() == 0) return new double[] { x, y, l };
        c = query.charAt(0);
        boolean isWGS = c == 'W';

        while ('0' > c || c > '9') {
            i = query.indexOf(" ");
            if (i < 0) break;
            query = query.substring(i).trim();
            if (query.length() > 0) c = query.charAt(0);
            else break;
        }

//        Log.d(TAG, "parseQuery("+query+")");

        if (query.length() > 0 && (i = query.indexOf(" ")) > 0) {
            String q = query.substring(0, i);
            if (isWGS) {
                x = Tools.auto2Deg(q, 0);
            } else {
                x = Double.parseDouble(q);
            }
            query = query.substring(i).trim();
        } else return new double[] {};
        if (query.length() > 0 && (i = query.indexOf(" ")) > 0) {
            String q = query.substring(0, i);
            if (isWGS) {
                y = Tools.auto2Deg(q, 0);
            } else {
                y = Double.parseDouble(q);
            }
            query = query.substring(i).trim();
        } else return new double[] { x };
        if (query.length() > 0) {
            return new double[]{x, y, Double.parseDouble(query)};
        }
        else return new double[] { x, y };
    }
    private String loadHtml(String fileName)
    {
        try {
            InputStream is = getAssets().open(fileName);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            warn("Cannot load "+fileName);
        }
        return "";
    }

    private void warn(String msg) { Toast.makeText(this, msg, Toast.LENGTH_SHORT).show(); }
    private void cleanTvResult() {
        mTvResult.setText("");
    }
}
