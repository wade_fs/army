package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import java.util.Arrays;

/**
 * Created by wade on 2017/3/10.
 */

public class AngleTools extends Activity {
    final static String TAG="MyLog";
    EditText mDeg, mD, mM, mS, mRad, mMil;
    Button mBtClear, mBtCalc;
    Button mBtClear2, mBtCalc2;
    EditText mExp;
    TextView mResult;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.angle_tools);
        mDeg = (EditText)findViewById(R.id.etDeg);
        mD = (EditText)findViewById(R.id.etATD);
        mM = (EditText)findViewById(R.id.etATM);
        mS = (EditText)findViewById(R.id.etATS);
        mRad = (EditText)findViewById(R.id.etRad);
        mMil = (EditText)findViewById(R.id.etMil);

        mExp = (EditText)findViewById(R.id.etExp);
        mResult = (TextView)findViewById(R.id.tvAtResult);

        mBtClear = (Button)findViewById(R.id.btAtClear);
        mBtClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mBtCalc = (Button)findViewById(R.id.btAtCalc);
        mBtCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMil.isFocused()) {
                    calcMil();
                } else if (mRad.isFocused()) {
                    calcRad();
                } else if (mDeg.isFocused()) {
                    calcDeg();
                } else {
                    calcDms();
                }
            }
        });
        mBtClear2 = (Button)findViewById(R.id.btAtClear2);
        mBtClear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset2();
            }
        });
        mBtCalc2 = (Button)findViewById(R.id.btAtCalc2);
        mBtCalc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String exp = mExp.getText().toString();
                double deg = Tools.auto2Deg(exp, 0);
                while (deg < 0.0 || deg >= 360.0) deg = (deg + 360.0)%360.0;
                if (Tools.parseDoubleException) Log.d("MyLog","請檢查運算式是否有誤");
                else mResult.setText("結果:\n"+String.format("%.6f", deg)+"度("+Tools.Deg2DmsStr2(deg)+")\n"+
                        String.format("%.3f", Tools.Deg2Mil(deg))+"密位");
            }
        });

        mDeg.setOnEditorActionListener(new TextView.OnEditorActionListener() {
           @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
               if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                   calcDeg();
                   disappear(v);
                   return true;
               }
               return false;
           }
        });
        mDeg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    calcDeg();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });

        mD.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    calcDms();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mD.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    calcDms();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mM.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    calcDms();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mM.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    calcDms();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mS.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    calcDms();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mS.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    calcDms();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mRad.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    calcRad();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mRad.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    calcRad();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });

        mMil.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    calcMil();
                    disappear(v);
                    return true;
                }
                return false;
            }
        });
        mMil.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    calcMil();
                    disappear(v);
                    return true;
                } else return false;
            }
        });
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        mDeg.setText("");
        mD.setText("");
        mM.setText("");
        mS.setText("");
        mRad.setText("");
        mMil.setText("");
        mD.requestFocus();
    }
    private void reset2() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset3();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset3() {
        mExp.setText("");
        mResult.setText("");
        mExp.requestFocus();
    }
    private void calcMil() {
        String input = mMil.getText().toString();
        if (input.length() == 0) showMsg("請確認輸入密位");
        else {
            double m = parseDouble(mMil, input);
            mDeg.setText(String.format("%.6f", Tools.Mil2Deg(m)));
            double[] dms = Tools.Deg2Dms3(Tools.Mil2Deg(m));
            mD.setText(String.format("%s%d", (dms[0]==-1?"-":""), (int)(dms[1])));
            mM.setText(String.format("%d", (int)(dms[2])));
            mS.setText(String.format("%.2f", dms[3]));
            mRad.setText(String.format("%.4f", Tools.Mil2Rad(m)));
        }
    }
    private void calcRad() {
        String exp = mRad.getText().toString();
        if (exp.length() == 0) showMsg("請確認輸入徑度");
        else {
            exp = exp.toLowerCase();
            exp = exp.replaceAll("π", "3.141592653589793");
            exp = exp.replaceAll("pi", "3.141592653589793");
            double r = parseDouble(mRad, exp);
            mDeg.setText(String.format("%.6f", Tools.Rad2Deg(r)));
            double[] dms = Tools.Deg2Dms3(Tools.Rad2Deg(r));
            mD.setText(String.format("%s%d", (dms[0]==-1?"-":""), (int)(dms[1])));
            mM.setText(String.format("%d", (int) dms[2]));
            mS.setText(String.format("%.2f", dms[3]));
            mMil.setText(String.format("%.2f", Tools.Rad2Mil(r)));
        }
    }
    private void calcDeg() {
        String input = mDeg.getText().toString();
        if (input.length() == 0) showMsg("請確認輸入度數");
        else {
            double d = parseDouble(mDeg, input);
            double[] dms = Tools.Deg2Dms3(d);
            mD.setText(String.format("%s%d", (dms[0]==-1?"-":""), (int)(dms[1])));
            mM.setText(String.format("%d", (int) dms[2]));
            mS.setText(String.format("%.2f", dms[3]));
            mRad.setText(String.format("%.4f", Tools.Deg2Rad(d)));
            mMil.setText(String.format("%.2f", Tools.Deg2Mil(d)));
        }
    }
    private void calcDms() {
        String D = mD.getText().toString();
        String M = mM.getText().toString();
        String S = mS.getText().toString();
        if (D.length() == 0 || M.length() == 0 || S.length() == 0) {
            showMsg("請確認輸入度分秒各項數據");
        } else {
            double d = Tools.Dms2Deg(D + "^" + M + "'" + S + "\"");
            mDeg.setText(String.format("%.6f", d));
            mRad.setText(String.format("%.4f", Tools.Deg2Rad(d)));
            mMil.setText(String.format("%.2f", Tools.Deg2Mil(d)));
        }
    }
    double parseDouble(EditText et, String s) {
        double d = Tools.parseDouble(s);
        et.setText(""+d);
        return d;
    }
    void disappear(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}
