package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PadsProgram extends Activity
{
    private final String TAG="MyLog";
    private final int LEFT = 1;
    private final int RIGHT = 2;
    private static PadsProgram mainActivity;
    private int btIds[], ids[], tvIds[];
    private List<EditText> ets;
    private List<TextView> tvs;

    Button btVisible;
    boolean isVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pads_program);
        mainActivity = (PadsProgram)this;
        btVisible = (Button)findViewById(R.id.btVisible);

        ids = new int[] {
            R.id.etOlX, R.id.etOlY, R.id.etOlH, R.id.etOlS, R.id.etOlA, R.id.etOlV,
            R.id.etOrX, R.id.etOrY, R.id.etOrH, R.id.etOrS, R.id.etOrA, R.id.etOrV,
        };
        ets = new ArrayList<>();
        for (int id:ids) {
            ets.add((EditText)findViewById(id));
        }
        tvIds = new int[] {
                R.id.tvOlX, R.id.tvOlY, R.id.tvOlH, R.id.tvOlS, R.id.tvOlA, R.id.tvOlV,
                R.id.tvOrX, R.id.tvOrY, R.id.tvOrH, R.id.tvOrS, R.id.tvOrA, R.id.tvOrV,
        };
        tvs = new ArrayList<>();
        for (int id:tvIds) {
            tvs.add((TextView)findViewById(id));
        }
        reset1();
        restore(0);
    }
    protected void addField(int idx, String value) {
        EditText et = ets.get(idx);
        if (et != null) {
            value = value.trim();
            et.setText(value);
        } else Log.d(TAG, "addField("+idx+","+value+") failed!");
    }
    private boolean readLL(int idx, BufferedReader bufferedReader) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) return false;
        line += " ";
        String[] res = line.split(",");
        if (res.length == 3) {
            addField(idx, res[2]);
            return true;
        }
        return false;
    }
    private void setIsVisible(boolean v) {
        if (v) {
            ets.get(3).setVisibility(View.INVISIBLE);
            tvs.get(3).setVisibility(View.INVISIBLE);
            ets.get(9).setVisibility(View.INVISIBLE);
            tvs.get(9).setVisibility(View.INVISIBLE);
            tvs.get(4).setText("左觀測所外角");
            tvs.get(10).setText("右觀測所內角");
            btVisible.setText("可通視");
        }
        else {
            ets.get(3).setVisibility(View.VISIBLE);
            tvs.get(3).setVisibility(View.VISIBLE);
            ets.get(9).setVisibility(View.VISIBLE);
            tvs.get(9).setVisibility(View.VISIBLE);
            tvs.get(4).setText("方位基準點至目標水平角");
            tvs.get(10).setText("方位基準點至目標水平角");
            btVisible.setText("不可通視");
        }
        this.isVisible = v;
    }
    public void onClickVisible(View view) {
        restore(isVisible?0:1);
    }
    private void restore(int visible) {
        File file = new File(Environment.getExternalStorageDirectory()+"/save-PADS"+visible+".txt");
        try {
            FileReader f = new FileReader(file);
            if (f == null) return;
            BufferedReader bufferedReader = new BufferedReader(f);
            if (bufferedReader == null) return;
            int idx = 0;
            while (readLL(idx++, bufferedReader)) {}
            bufferedReader.close();
            f.close();
        } catch (FileNotFoundException e1) {
        } catch (IOException e) {
        }
        setIsVisible(visible == 1);
    }
    private void save(int visible) {
        try {
            File file = new File(Environment.getExternalStorageDirectory()+"/save-PADS"+visible+".txt");
            FileWriter f  = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(f);

            for (int id=0; id<ids.length; id++) {
                bufferedWriter.write(String.format(Locale.CHINESE, "%s,%s,%s\n",
                        (id<6?"左":"右"), tvs.get(id).getText(), ets.get(id).getText()));
            }
            bufferedWriter.close();
            f.close();
        } catch (IOException e) {
            Log.d(TAG, "Save() failed");
        }
    }
    double[] result=new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0}; // POL(o1, o2).d, POL(o1, o2).a, L, R, <O(R)-T, <O(L)-T, MEAN(x, y,h)
    private void preview() {
        TextView textView = new TextView(this);
        String res = "";
        res += String.format(Locale.CHINESE, "右觀測所至左觀測所距離=%.2f\n右觀測所至左觀測所方位角=%s\n左觀目距離=%.2f, 右觀目距離=%.2f\n",
                result[0], Tools.ang2Str(result[1]),
                result[2], result[3]);
        res += String.format(Locale.CHINESE, "左觀目方位角 = %s, 右觀目方位角=%s\n目標平均坐標、標高(x,y,h)=(%.2f, %.2f, %.2f)\n",
                Tools.ang2Str(result[5]), Tools.ang2Str(result[4]),
                result[6], result[7], result[8]);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        textView.setText(res);
        textView.setScroller(new Scroller(this));
        textView.setVerticalScrollBarEnabled(true);
        textView.setMovementMethod(new ScrollingMovementMethod());
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("成果快速預覽")
                .setView(textView)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
        dialog.getWindow().setLayout(size.x, size.y);
    }
    private double inputDouble(EditText et) {
        String input = et.getText().toString();
        if (input.length() == 0) return 0;
        else return Tools.parseDouble(input);
    }
    private double inputDegree(EditText et) {
        String input = et.getText().toString();
        if (input.length() == 0) return 0;
        else return Tools.auto2Deg(input, 0);
    }
    private double inputDegree(String ang) {
        return Tools.auto2Deg(ang, 0);
    }
    private void Warn(String msg, int l) {
        if (l != 2) Log.d(TAG, "PadsProgram: "+msg);
        if (l != 1) Toast.makeText(mainActivity, "PadsProgram: "+msg, Toast.LENGTH_LONG).show();
    }
    private boolean empty(EditText e) {
        if (e == null || e.getText().toString().trim().length() == 0) return true;
        return false;
    }
    // PADS: 方位確認系統 Position and Azimuth Determining System： s1, ep1 屬於 O1, s2, ep2 屬於 O2/O3
    public void calc(View view) {
        save(isVisible?1:0);
        double olx = inputDouble(ets.get(0));
        double oly = inputDouble(ets.get(1));
        double olh = inputDouble(ets.get(2));
        double ols = inputDegree(ets.get(3));
        double ola = inputDegree(ets.get(4));  // ola 是左角(外角), ora 是右角(內角)

        String ang = ets.get(5).getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double olv = inputDegree(ang);

        double orx = inputDouble(ets.get(6));
        double ory = inputDouble(ets.get(7));
        double orh = inputDouble(ets.get(8));
        double ors = inputDegree(ets.get(9));
        double ora = inputDegree(ets.get(10));  // ola 是左角(外角), ora 是右角(內角)

        ang = ets.get(11).getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double orv = inputDegree(ang);

        for (EditText e : ets) {
            if (e.getId() == R.id.etOlV || e.getId() == R.id.etOrV) continue;
            if (empty(e)) {
                if (isVisible && e.getId() != ets.get(3).getId() && e.getId() != ets.get(9).getId() || !isVisible) {
                    Warn("請檢查欄位是否都有填值", 0);
                    return;
                }
            }
        }

        double[] res = Tools.POLd(oly - ory, olx - orx);
        result[0] = res[0];
        result[1] = res[1];
        double td = res[0], ta=res[1]; // ta = <OR->OL 方向角
        double a, b;
        if (isVisible) {
            a = ola;
            b = ora;
        } else {
            a = ols + ola - ta;
            b = ors + ora - ta;
        }
        a = (a + 720)%360.0;
        b = (b + 720)%360.0;
        boolean fl = !empty(ets.get(5));
        boolean fr = !empty(ets.get(11));
        if (!fl && ! fr) { fr = fl = true; }
        double e = td/Tools.SIN(a-b);
        double l = e * Tools.SIN(b);
        double r = e * Tools.SIN(a);
        result[2] = l;
        result[3] = r;
        // 這邊透過 PADS() 求目標的時候，因為兩邊長是算出來的，基本上是平距，因此垂直角應該換算成高低角
        if (45.0 < olv && olv <= 135) olv = (90.0-olv+360)%360;
        if (45.0 < orv && orv <= 135) orv = (90.0-orv+360)%360;
        if (l <= 0.0 || r <= 0.0) Warn("內角或外角有問題，無法求出目標正確數據，請檢查各欄位", 0);
        else {
            Log.d(TAG, String.format("orx=%.2f, ory=%.2f, orh=%.2f, ta=%.2f, r=%.2f, b=%.2f, orv=%.2f",
                    orx, ory, orh, ta, r, b, orv));
            Log.d(TAG, String.format("olx=%.2f, oly=%.2f, olh=%.2f, ta=%.2f, l=%.2f, s=%.2f, olv=%.2f",
                    olx, oly, olh, ta, l, a, olv));
            res = Tools.traverse3(orx, ory, orh, ta, r, b, orv);
            double xr = res[0], yr = res[1], hr = res[2];
            result[4] = res[3];
            res = Tools.traverse3(olx, oly, olh, ta, l, a, olv);
            double xl = res[0], yl = res[1], hl = res[2];
            result[5] = res[3];
            int c = 0;
            double h=0;
            if (fr) {
                h += hr;
                c++;
            }
            if (fl) {
                h += hl;
                c++;
            }
            result[6] = (xl+xr)/2;
            result[7] = (yl+yr)/2;
            result[8] = c != 0 ? h/c : (hr+hl)/2;
//            Log.d(TAG, String.format("左: (%.2f,%.2f,%.2f)%s | 右: (%.2f,%.2f,%.2f)%s => (%.2f,%.2f,%.2f)",
//                    xl, yl, hr, (fl?"V":"X"),
//                    xr, yr, hl, (fr?"V":"X"),
//                    (xl+xr)/2, (yl+yr)/2, h/c));
            preview();
        }
    }
    private boolean checkInput(EditText A, EditText D, EditText V) {
        if (A != null && A.getText().toString().length() == 0) return false;
        if (D != null && D.getText().toString().length() == 0) return false;
        if (V != null && V.getText().toString().length() == 0) return false;
        return true;
    }
    public void reset(View view) {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (EditText et : ets) {
            et.setText("");
        }
        ets.get(0).requestFocus();
    }
}
