package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Proj;
import com.imobile.libs.Tools;
import com.imobile.libs.XYZ;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
// 坐標系統及高程基準轉換
// http://gis.thl.ncku.edu.tw/coordtrans/coordtrans.aspx
// http://www.sunriver.com.tw/taiwanmap/grid_tm2_convert.php
// UTM-6 http://www.earthpoint.us/Convert.aspx
public class CoordinateTransform extends AppCompatActivity {
    CoordinateTransform mThis;
    Button btCalc, btReset, btData;
    Spinner mSpSourceType, mSpArea, mSpCorFormat;
    ArrayAdapter<String> arrayAdapterSource;
    ArrayAdapter<String> arrayAdapterCorFormat;
    ArrayAdapter<String> arrayAdapterArea;
    String[] stCorFormatWGS84, stCorFormatTWD97, stArea1, stArea2;
    ArrayList<String> lst, lstArea;
    Spinner mSpHType;
    TextView tvX, tvY, tvH, tvResult;
    EditText mEtSourceX, mEtSourceY, mEtSourceH;
    EditText mEtSourceXD, mEtSourceXM, mEtSourceXS;
    EditText mEtSourceYD, mEtSourceYM, mEtSourceYS;
    LinearLayout ll1, ll2;
    Proj mProj;
    int iSourceType=0, iCorFormat=0, iArea=0, iHightType=0, degreeMode=0;
    XYZ xyz;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinatetransform);
        mThis = this;
        mProj = new Proj();
        xyz = ArtilleryBattalion.getXYZ();

        ll1 = (LinearLayout)findViewById(R.id.ll1);
        ll2 = (LinearLayout)findViewById(R.id.ll2);
        ll1.setVisibility(View.VISIBLE);
        ll2.setVisibility(View.GONE);
        tvX = (TextView)findViewById(R.id.tvSourceX);
        tvY = (TextView)findViewById(R.id.tvSourceY);
        tvH = (TextView)findViewById(R.id.tvSourceH);
        tvResult = (TextView)findViewById(R.id.etResultCoordinateTransform);

        mEtSourceX = (EditText)findViewById(R.id.etSourceX);
        mEtSourceY = (EditText)findViewById(R.id.etSourceY);
        mEtSourceXD =(EditText)findViewById(R.id.etSourceXD);
        mEtSourceXM =(EditText)findViewById(R.id.etSourceXM);
        mEtSourceXS =(EditText)findViewById(R.id.etSourceXS);
        mEtSourceYD =(EditText)findViewById(R.id.etSourceYD);
        mEtSourceYM =(EditText)findViewById(R.id.etSourceYM);
        mEtSourceYS =(EditText)findViewById(R.id.etSourceYS);
        mEtSourceH = (EditText)findViewById(R.id.etSourceH);

        btCalc = (Button)findViewById(R.id.btCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });

        btReset = (Button)findViewById(R.id.btReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        mSpSourceType = (Spinner)findViewById(R.id.spSourceType);
        arrayAdapterSource = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.stCRSTypes));
        arrayAdapterSource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpSourceType.setAdapter(arrayAdapterSource);
        mSpSourceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                iSourceType = position;
                lst.remove(2);
                lst.remove(1);
                lst.remove(0);
                lstArea.remove(1);
                lstArea.remove(0);
                if (position == 0) {
                    lst.add(0, stCorFormatWGS84[0]);
                    lst.add(1, stCorFormatWGS84[1]);
                    lst.add(2, stCorFormatWGS84[2]);
                    lstArea.add(0, stArea1[0]);
                    lstArea.add(1, stArea1[1]);
                } else {
                    lst.add(0, stCorFormatTWD97[0]);
                    lst.add(1, stCorFormatTWD97[1]);
                    lst.add(2, stCorFormatTWD97[2]);
                    lstArea.add(0, stArea2[0]);
                    lstArea.add(1, stArea2[1]);
                }
                arrayAdapterCorFormat.notifyDataSetChanged();
                arrayAdapterArea.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mSpSourceType.setSelection(0);
            }
        });

        stArea1 = getResources().getStringArray(R.array.stArea);
        stArea2 = getResources().getStringArray(R.array.stArea2);
        mSpArea = (Spinner)findViewById(R.id.spArea);
        lstArea = new ArrayList<>(Arrays.asList(stArea1));
        arrayAdapterArea = new ArrayAdapter<String>(this, R.layout.spinner_item, lstArea);
        arrayAdapterArea.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpArea.setAdapter(arrayAdapterArea);
        mSpArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                iArea = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mSpSourceType.setSelection(0);
            }
        });

        stCorFormatWGS84 = getResources().getStringArray(R.array.stCorFormatWGS84);
        stCorFormatTWD97 = getResources().getStringArray(R.array.stCorFormatTWD97);
        mSpCorFormat = (Spinner)findViewById(R.id.spCorFormat);
        lst = new ArrayList<>(Arrays.asList(stCorFormatWGS84));
        arrayAdapterCorFormat = new ArrayAdapter<String>(this, R.layout.spinner_item, lst);
        arrayAdapterCorFormat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpCorFormat.setAdapter(arrayAdapterCorFormat);
        mSpCorFormat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                iCorFormat = position;
                if (position == 0) {
                    ll1.setVisibility(View.VISIBLE);
                    ll2.setVisibility(View.GONE);
                    tvX.setText("X=");
                    tvY.setText("Y=");
                    mSpArea.setEnabled(true);
                    lstArea.remove(1);
                    lstArea.remove(0);
                    if (iSourceType == 0) {
                        lstArea.add(0, stArea1[0]);
                        lstArea.add(1, stArea1[1]);
                    } else {
                        lstArea.add(0, stArea2[0]);
                        lstArea.add(1, stArea2[1]);
                    }
                    arrayAdapterArea.notifyDataSetChanged();
                } else if (position == 1) {
                    ll1.setVisibility(View.GONE);
                    ll2.setVisibility(View.VISIBLE);
                    tvX.setText("經度=");
                    tvY.setText("緯度=");
                    mSpArea.setEnabled(false);
                } else if (position == 2) {
                    ll1.setVisibility(View.VISIBLE);
                    ll2.setVisibility(View.GONE);
                    tvX.setText("經度=");
                    tvY.setText("緯度=");
                    mSpArea.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mSpSourceType.setSelection(0);
            }
        });

        mSpHType = (Spinner)findViewById(R.id.spHType);
        ArrayAdapter<CharSequence> arrayAdapterH = new ArrayAdapter<CharSequence>(this, R.layout.spinner_item, getResources().getStringArray(R.array.stHTypes));
        arrayAdapterH.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpHType.setAdapter(arrayAdapterH);
        mSpHType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                iHightType = position;
                if (position == 0) {
                    tvH.setText("正高=");
                } else {
                    tvH.setText("橢球高=");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mSpSourceType.setSelection(0);
            }
        });
        ArrayAdapter<CharSequence> arrayAdapterTarget = ArrayAdapter.createFromResource(this, R.array.stCRSTypes, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterTarget.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        btData = (Button)findViewById(R.id.btData);
        btData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CoordinateTransform.this, ControlPoint.class));
            }
        });
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        mEtSourceX.setText("");
        mEtSourceY.setText("");
        mEtSourceH.setText("");
        mEtSourceXD.setText("");
        mEtSourceXM.setText("");
        mEtSourceXS.setText("");
        mEtSourceYD.setText("");
        mEtSourceYM.setText("");
        mEtSourceYS.setText("");
        if (mSpCorFormat.getSelectedItemPosition() == 1) {
            mEtSourceXD.requestFocus();
        } else {
            mEtSourceX.requestFocus();
        }
    }
    private void warn(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    private String getH(double x, double y, double h) {
        double H;
        if (x < y) H = xyz.getHeight(x, y, 0);
        else H = xyz.getHeight(y, x, 0);
        if (iHightType == 0) return String.format("橢球高 = %.2f", (h+H));
        else  return String.format("正高 = %.2f", (h-H));
    }

    private boolean checkInput() {
        if (mSpCorFormat.getSelectedItemPosition() == 1) {
            if (mEtSourceXD.getText().toString().equals("")) return false;
            if (mEtSourceXM.getText().toString().equals("")) return false;
            if (mEtSourceXS.getText().toString().equals("")) return false;
            if (mEtSourceYD.getText().toString().equals("")) return false;
            if (mEtSourceYM.getText().toString().equals("")) return false;
            if (mEtSourceYS.getText().toString().equals("")) return false;
        } else {
            if (mEtSourceX.getText().toString().equals("")) return false;
            if (mEtSourceY.getText().toString().equals("")) return false;
        }
        return true;
    }

    private final static double LONG0TW=121.0;
    private final static double LONG0PH=119.0;
    private final static double deg2rad = Math.PI / 180.0;
    private final static double rad2deg = 180.0 / Math.PI;
    private TMParameter tm = new TMParameter();
    private double dx = tm.getDx();
    private double dy = tm.getDy();
    private double lon0 = tm.getLon0(LONG0TW);
    private double k0 = tm.getK0();
    private double a = tm.getA();
    private double b = tm.getB();
    private double e = Math.sqrt((1 - Math.pow(b, 2) / Math.pow(a, 2)));
    private Ellipsoid[] ellipsoid = new Ellipsoid[]{
            new Ellipsoid(-1, "Placeholder", 0, 0),
            new Ellipsoid(1, "Airy", 6377563, 0.00667054),
            new Ellipsoid(2, "Australian National", 6378160, 0.006694542),
            new Ellipsoid(3, "Bessel 1841", 6377397, 0.006674372),
            new Ellipsoid(4, "Bessel 1841 (Nambia) ", 6377484, 0.006674372),
            new Ellipsoid(5, "Clarke 1866", 6378206, 0.006768658),
            new Ellipsoid(6, "Clarke 1880", 6378249, 0.006803511),
            new Ellipsoid(7, "Everest", 6377276, 0.006637847),
            new Ellipsoid(8, "Fischer 1960 (Mercury) ", 6378166, 0.006693422),
            new Ellipsoid(9, "Fischer 1968", 6378150, 0.006693422),
            new Ellipsoid(10, "GRS 1967", 6378160, 0.006694605),
            new Ellipsoid(11, "GRS 1980", 6378137, 0.00669438),
            new Ellipsoid(12, "Helmert 1906", 6378200, 0.006693422),
            new Ellipsoid(13, "Hough", 6378270, 0.00672267),
            new Ellipsoid(14, "International", 6378388, 0.00672267),
            new Ellipsoid(15, "Krassovsky", 6378245, 0.006693422),
            new Ellipsoid(16, "Modified Airy", 6377340, 0.00667054),
            new Ellipsoid(17, "Modified Everest", 6377304, 0.006637847),
            new Ellipsoid(18, "Modified Fischer 1960", 6378155, 0.006693422),
            new Ellipsoid(19, "South American 1969", 6378160, 0.006694542),
            new Ellipsoid(20, "WGS 60", 6378165, 0.006693422),
            new Ellipsoid(21, "WGS 66", 6378145, 0.006694542),
            new Ellipsoid(22, "WGS-72", 6378135, 0.006694318),
            new Ellipsoid(23, "WGS-84", 6378137, 0.00669438)
    };
    public class TMParameter{
        public double getDx(){ return 250000; }
        public double getDy(){ return 0; }
        public double getLon0(double lon){ return lon * Math.PI / 180.0; }
        public double getK0(){ return 0.9999; }
        public double getA(){ return 6378137.0; }
        public double getB(){ return 6356752.314245; }
        public double getE() { return 0.08181919084; } // 0.0818201799960599; 來自
    }
    public class Ellipsoid {
        int id;
        String ellipsoidName;
        double EquatorialRadius;
        double eccentricitySquared;

        Ellipsoid(int Id, String name, double radius, double ecc)
        {
            id = Id; ellipsoidName = name;
            EquatorialRadius = radius;
            eccentricitySquared = ecc;
        }
    }

    public double[] LL2UTM(double lon, double lat, int zone) {
        double a = 6378137;
        double eccSquared = 0.00669438;
        double ee = e * e; // == 0.00669438
        k0 = 0.9996;
        double LongOrigin, eccPrimeSquared;
        double N, T, C, A, M;
        double LongTemp = lon+180.0-Math.floor((lon+180.0)/360.0)*360.0-180.0;
        double LatRad = lat * deg2rad;
        double LongRad = LongTemp * deg2rad;
        double LongOriginRad;
        int ZoneNumber = zone == 0?(int)Math.floor((LongTemp + 180) / 6) + 1:zone;

        LongOrigin = (ZoneNumber - 1) * 6 - 180.0 + 3;
        LongOriginRad = LongOrigin * deg2rad;

        eccPrimeSquared = (ee) / (1-ee);
        N = a/Math.sqrt(1-ee*Math.sin(LatRad) * Math.sin(LatRad));
        T = Math.tan(LatRad) * Math.tan(LatRad);
        C = eccPrimeSquared * Math.cos(LatRad) * Math.cos(LatRad);
        A = Math.cos(LatRad) * (LongRad - LongOriginRad);

        M = a * (( 1-ee/4 - 3 * ee*ee / 64 - 5 * ee * ee * ee / 256) * LatRad
                - (3*ee / 8 + 3 * ee * ee / 32 + 45 * ee * ee * ee / 1024) * Math.sin(2*LatRad)
                + (15 * ee*ee / 256 + 45 * ee*ee*ee/1024)*Math.sin(4*LatRad)
                - (35*ee*ee*ee/3072) * Math.sin(6*LatRad));
        double UTM0 = (k0 *N*(A+(1-T+C)*A*A*A/6 +
                (5-18*T+T*T+72*C -58*eccPrimeSquared)*A*A*A*A*A/120) + 500000.0);
        double UTM1 = (k0*(M+N*Math.tan(LatRad)*(A*A/2+(5-T+9*C+4*C*C)*A*A*A*A/24
                +(61-58*T+T*T+600*C-330*eccPrimeSquared)*A*A*A*A*A*A/720)));

        return new double[] {UTM0, UTM1, ZoneNumber};
    }

    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請確認已填入上方各欄位數值，再按計算", Toast.LENGTH_LONG).show();
            return;
        }
        boolean calcHeight = mEtSourceH.getText().toString().length() !=0;

        double x, y, h, x97, y97;
        String expX, expY, expH;
        if (iCorFormat == 1) { // 度分秒
            expX = Tools.Dms2Deg(mEtSourceXD.getText().toString(), mEtSourceXM.getText().toString(), mEtSourceXS.getText().toString());
            expY = Tools.Dms2Deg(mEtSourceYD.getText().toString(), mEtSourceYM.getText().toString(), mEtSourceYS.getText().toString());
        } else {
            expX = mEtSourceX.getText().toString();
            expY = mEtSourceY.getText().toString();
        }
        expH = mEtSourceH.getText().toString();
        String msg = "";
        h = Tools.parseDouble(expH);
//      120.982025, 23.973875 ==> 120.58'55.271", 23.58'25.927" ==>
//      51Q 294679mE 2652803mN ==> TM-2 248170.29, 2652129.28
        x = Tools.parseDouble(expX);
        y = Tools.parseDouble(expY);
        if (iSourceType == 0) { // WGS84
            if (iCorFormat == 0) { // 六度分帶
                // 先轉經緯度
                double[] resLL = mProj.UTM2LL(x, y, iArea==0?51:50, 23);
                msg += String.format(Locale.CHINESE, "%s (經度,緯度)\n\t度分秒 = (E %s,\t N %s)\n\t度小數 = (E %.6f,\t N %.6f)\n",
                        (iArea == 0)?"臺灣":"澎金馬",
                        Tools.Deg2DmsStr2(resLL[0]), Tools.Deg2DmsStr2(resLL[1]), resLL[0], resLL[1]);
                // 再轉 TM-2
                double[] resTM2 = mProj.LL2TM2(resLL[0], resLL[1]);
                msg += String.format(Locale.CHINESE, "%sTWD97 TM-2 %s (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        calcHeight?getH(resTM2[0], resTM2[1], h)+"\n":"", (iArea == 0)?"臺灣":"澎金馬",
                        resTM2[0], resTM2[1]);
                // 新增需求，跨區
                double[] resUTM = mProj.LL2UTM(resLL[0], resLL[1], iArea+50);
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %d帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        iArea+50,
                        resUTM[0], resUTM[1]);
            } else { //  經緯度轉...
                double[] resTM2 = mProj.LL2TM2(x, y);
                msg += String.format(Locale.CHINESE, "%s經度=%s(%.6f), 緯度=%s(%.6f)\n",
                        calcHeight?getH(resTM2[0], resTM2[1], h)+", ":"",
                        Tools.Deg2DmsStr2(x), x, Tools.Deg2DmsStr2(y), y);
                double[] resUTM = mProj.LL2UTM(x, y, 0);  // 120.982025, 23.973875 ==> 51Q 294679mE 2652803mN
                iArea = resUTM[2]>50.5?0:1;
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %s帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        (iArea == 0)?"51":"50",
                        resUTM[0], resUTM[1]);
                double[] resUTM2 = mProj.LL2UTM(x, y, iArea+50);
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %d帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        iArea+50,
                        resUTM2[0], resUTM2[1]);
                // 再轉成二度分帶
                msg += String.format(Locale.CHINESE, "TWD97 TM-2 %s (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        (iArea == 0)?"臺灣":"澎金馬",
                        resTM2[0], resTM2[1]);
            }
        } else if (iSourceType == 1) { // TM-2-TW 97
            if (iCorFormat == 0) { // 二度分帶
                // 先轉經緯度
                double[] resTM2 = mProj.TM2LL(x, y, iArea==0?121:119);
                msg += String.format(Locale.CHINESE, "%s%s (經度,緯度)\n\t度分秒 = (E %s, N %s)\n\t度小數 = (E %.6f, N %.6f)\n",
                        calcHeight?getH(x, y, h)+"\n":"",
                        (iArea == 0)?"臺灣":"澎金馬",
                        Tools.Deg2DmsStr2(resTM2[0]), Tools.Deg2DmsStr2(resTM2[1]), resTM2[0], resTM2[1]);
                // 再轉 UTM-6
                double[] resUTM = LL2UTM(resTM2[0], resTM2[1], 50);
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %s帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        (int)(resUTM[2]),
                        resUTM[0], resUTM[1]);
                double[] resUTM2 = LL2UTM(resTM2[0], resTM2[1], 51);
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %d帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        (int)(resUTM2[2]),
                        resUTM2[0], resUTM2[1]);
            } else { //  經緯度轉...
                // 先轉成 TM-2
                double[] resTM2 = mProj.LL2TM2(x, y);  // 120.982025, 23.973875 ==> 51Q 294679mE 2652803mN
                iArea = resTM2[2]>120.0?0:1;
                msg += String.format(Locale.CHINESE, "%s經度=%s(%.6f), 緯度=%s(%.6f)\nTWD97 TM-2 %s (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        calcHeight?getH(resTM2[0], resTM2[1], h)+", ":"",
                        Tools.Deg2DmsStr2(x), x, Tools.Deg2DmsStr2(y), y,
                        (iArea == 0)?"臺灣":"澎金馬",
                        resTM2[0], resTM2[1]);
                // 再轉成六度分帶
                double[] resUTM = mProj.LL2UTM(x, y, 50);
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %s帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        "50",
                        resUTM[0], resUTM[1]);
                double[] resUTM2 = mProj.LL2UTM(x, y, 51);
                msg += String.format(Locale.CHINESE, "WGS84 UTM-6 %d帶 (X,Y) = (E %.2f (公尺), N %.2f (公尺))\n",
                        51,
                        resUTM2[0], resUTM2[1]);
            }
        }

        tvResult.setText(msg);
    }
}
