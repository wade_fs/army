package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import java.util.Arrays;

public class P3AntiIntersection extends Activity {
    private final String TAG="MyLog";

    Button btReset, btCalc, btDegreeMode;
    TextView mTvResult;
    int ids[];
    EditText ets[];
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.p3_anti_intersection);

        ids = new int[]{
            R.id.etP3XA, R.id.etP3YA, R.id.etP3HA,
            R.id.etP3XC, R.id.etP3YC, R.id.etP3HC,
            R.id.etP3XB, R.id.etP3YB, R.id.etP3HB,
            R.id.etP3R,  R.id.etP3S,  R.id.etP3PA, R.id.etP3PB,
            R.id.etP3EP, R.id.etP3CA, R.id.etP3CB,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btP3DegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });
        btCalc = (Button)findViewById(R.id.btP3Calc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });

        btReset = (Button)findViewById(R.id.btResetP3);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvResultP3);
    }

    private boolean checkInput(int s) {
        for (int i=0; i<((s==1)?11:13); i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }
    public void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
        }
        ets[0].requestFocus();
    }
    private double inputDegree(EditText exp) {
        return Tools.auto2Deg(exp.getText().toString(), degreeMode);
    }
    private double inputDegree(String ang) {
        return Tools.auto2Deg(ang, degreeMode);
    }
    public void calc() {
        try {
            if (!checkInput(1)) {
                Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
                return;
            }

            double[] X = new double[5];
            double[] Y = new double[5];
            double[] H = new double[5];

            double r = inputDegree(ets[9]);
            double S = inputDegree(ets[10]);

            X[1] = Tools.parseDouble(ets[0]);
            Y[1] = Tools.parseDouble(ets[1]);
            H[1] = Tools.parseDouble(ets[2]);
            X[3] = Tools.parseDouble(ets[3]);
            Y[3] = Tools.parseDouble(ets[4]);
            H[3] = Tools.parseDouble(ets[5]);
            X[2] = Tools.parseDouble(ets[6]);
            Y[2] = Tools.parseDouble(ets[7]);
            H[2] = Tools.parseDouble(ets[8]);

            //60
            double[] dPOL = Tools.POLd(Y[3] - Y[1], X[3] - X[1]);
            double D = dPOL[0];
            double P = dPOL[1];
            dPOL = Tools.POLd(Y[3] - Y[2], X[3] - X[2]);
            double XX = dPOL[0];
            double YY = dPOL[1];
            double C = P - YY; C = C - 360.0 *Tools.INT(C/360.0);
            YY = YY - 360.0 * Tools.INT(YY/360.0);

            //130
            double K = Tools.ATAN(XX*Tools.SIN(r)/D/Tools.SIN(S));
            //140
            double U = (360.0 - r - S - C) / 2;
            double V = Tools.ATAN(Tools.TAN(U) * Tools.TAN(K-45));
            double A = U + V;
            double B = U - V;
            P = P + A;
            YY = YY - B;
            P = (P +360)%360;
            YY = (YY+360)%360;
            double L = D  * Tools.SIN(A+r) / Tools.SIN(r);
            double M = XX * Tools.SIN(B+S) / Tools.SIN(S);
            //210
            X[3] = X[1] + L * Tools.SIN(P);
            Y[3] = Y[1] + L * Tools.COS(P);
            X[4] = X[2] + M * Tools.SIN(YY);
            Y[4] = Y[2] + M * Tools.COS(YY);

            if (checkInput(2)) {
                String ang;
                ang = ets[11].getText().toString();
                if (ang.trim().equals("0")) ang = "1600";
                double Q = inputDegree(ang);
                ang = ets[12].getText().toString();
                if (ang.trim().equals("0")) ang = "1600";
                double F = inputDegree(ang);
                double EE = Tools.parseDouble(ets[13]);
                double SS = Tools.parseDouble(ets[14]);
                double TT = Tools.parseDouble(ets[15]);

                //270
                H[1] = Tools.len2Height(H[1], L, Q, EE, SS, -1)[0]; // H[1] + SS - EE - L * Tools.TAN(Q) - W;
                H[2] = Tools.len2Height(H[2], M, F, EE, TT, -1)[0]; // H[2] + TT - EE - M * Tools.TAN(F) - G;
                mTvResult.setText(String.format("P點橫坐標 %.2f, P點縱坐標 %.2f, P點標高 %.2f", ((X[3] + X[4]) / 2), ((Y[3] + Y[4]) / 2), ((H[1] + H[2]) / 2)));
            } else {
                mTvResult.setText(String.format("P點橫坐標 %.2f, P點縱坐標 %.2f", ((X[3] + X[4]) / 2), ((Y[3] + Y[4]) / 2)));
            }
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, "請確認是不是有輸入值，再按 EXE", Toast.LENGTH_SHORT).show();
        }
    }
}
