package com.imobile.artillerybattalion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import org.w3c.dom.Text;

public class TriangulationSurvey extends Activity {
    private final String TAG="MyLog";
    
    int ids[];
    EditText ets[];

    Button btCalc, btDegreeMode, btReset;
    TextView mTvResult;
    int degreeMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.triangulation_survey);
        ids = new int[]{
            R.id.etXR, R.id.etYR, R.id.etHR,
            R.id.etXL, R.id.etYL, R.id.etHL,
            R.id.etAT, R.id.etBR, R.id.etCL, 
            R.id.etBA, R.id.etCA, R.id.etER, R.id.etEL, R.id.etCT,
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btTSDegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

        btCalc = (Button)findViewById(R.id.btTSCalc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btTSReset);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        mTvResult = (TextView)findViewById(R.id.tvTSResult);
    }
    private boolean checkInput() {
        for (int i=0; i<11; i++) {
            if (ets[i].getText().length() == 0) return false;
        }
        return true;
    }
    private void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<ids.length; i++) {
            ets[i].setText("");
            ets[i].setEnabled(true);
        }
        ets[0].requestFocus();
    }

    private void calc() {
        if (!checkInput()) {
            Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
            return;
        }

        /// 先轉換角度
        double A = Tools.auto2Deg(ets[6].getText().toString(), degreeMode); // 6
        double B = Tools.auto2Deg(ets[7].getText().toString(), degreeMode); // 7
        double C = Tools.auto2Deg(ets[8].getText().toString(), degreeMode); // 8

        /// 設定其他值
        double X1 = Tools.parseDouble(ets[0]); // 0
        double Y1 = Tools.parseDouble(ets[1]); // 1
        double Z1 = Tools.parseDouble(ets[2]); // 2
        double X2 = Tools.parseDouble(ets[3]); // 3
        double Y2 = Tools.parseDouble(ets[4]); // 4
        double Z2 = Tools.parseDouble(ets[5]); // 5

        /// 接下來按照原始程式轉換公式

        // 1. 修正三個內角:
        double T = (180.0-A-B-C)/3;
        A += T; B += T; C += T;

        // 2. 由向量 LR^ 求外接圓直徑 d, 角 y, 2R = d/sin(A), 再求出兩邊長 l/r
        double res[] = Tools.POLd(Y2-Y1, X2-X1);
        double D = res[0] / Tools.SIN(A);
        double L = D * Tools.SIN(B);
        double R = D * Tools.SIN(C);

        double Y = res[1];

        // 3. 求 br, cl 的方位角
        // 　　從方位角的轉換相當複雜....
        B = Y + B;
        C = Y - 180 - C;

        // 4. 再用左邊＋右邊求目標的座標，後面要取平均, 其實應該是 x3 = x4, y3 = y4
        //    這邊是利用 sin(PI/2-a) = cos(a), cos(PI/2-a) = sin(a)
        double X3 = X1 + R * Tools.SIN(B);
        double Y3 = Y1 + R * Tools.COS(B);
        double X4 = X2 + L * Tools.SIN(C);
        double Y4 = Y2 + L * Tools.COS(C);

        String ang = ets[9].getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double F = Tools.auto2Deg(ang, degreeMode); // 9

        ang = ets[10].getText().toString();
        if (ang.trim().equals("0")) ang = "1600";
        double Q = Tools.auto2Deg(ang, degreeMode); // 10

        double S = Tools.parseDouble(ets[11]); // 11
               D = Tools.parseDouble(ets[12]); // 12
        double E = Tools.parseDouble(ets[13]); // 13

        // 5. 這個值很奇怪，只能理解是不同地圖系統的轉換值, z3, z4 也是分別從右邊/左邊求，後面再平均
        double Z3 = Tools.len2Height(Z1, R, F, S, E, +1)[0]; // Z1 + S - E + R * Tools.TAN(F) + W * R * R;
        double Z4 = Tools.len2Height(Z2, L, Q, D, E, +1)[0]; // Z2 + D - E + L * Tools.TAN(Q) + W * L * L;

        // 6. 結果就出來了
        mTvResult.setText(String.format("求點坐標 (%.2f, %.2f)\n求點標高 %.2f", (X3+X4)/2.0, (Y3+Y4)/2.0, (Z3+Z4)/2.0));
    }
}
