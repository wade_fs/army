package com.imobile.artillerybattalion;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.system.StructPollfd;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile.libs.Tools;

import org.w3c.dom.Text;

public class P1AntiIntersection extends Activity {
    private final String TAG="MyLog";

    TextView tvP1CAL;
    Button btLeft, btReset, btDegreeMode, btCalc;
    TextView mTvResult;

    int degreeMode = 0;
    int leftRight = 1; // 1:left, 2:right
    int ids[];
    EditText ets[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.p1_anti_intersection);

        ids = new int[]{
                R.id.etP1XA, R.id.etP1YA, R.id.etP1HA,
                R.id.etP1CAL, R.id.etP1BC,
                R.id.etP1BR, R.id.etP1CL,
                R.id.etP1CA, R.id.etP1BA,
                R.id.etP1CAP, R.id.etP1EBP, R.id.etP1ECP
        };
        ets = new EditText[ids.length];
        for (int i=0; i<ids.length; i++) {
            int res = ids[i];
            ets[i] = (EditText) findViewById(res);
        }
        btDegreeMode = (Button)findViewById(R.id.btP1DegreeMode);
        btDegreeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 0 auto, 1 Deg, 2 DMS, 3 Mil
                degreeMode = (degreeMode + 1) % 4;
                switch (degreeMode) {
                    case 0: btDegreeMode.setText("<自動"); break;
                    case 1: btDegreeMode.setText("度小數");break;
                    case 2: btDegreeMode.setText("度分秒");break;
                    case 3: btDegreeMode.setText("密位"); break;
                }
            }
        });

        tvP1CAL = (TextView)findViewById(R.id.tvP1CAL);
        btCalc = (Button)findViewById(R.id.btP1Calc);
        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc();
            }
        });
        btReset = (Button)findViewById(R.id.btResetP1);
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
        btLeft = (Button)findViewById(R.id.btP1Left);
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leftRight == 1) {
                    leftRight = 0;
                    tvP1CAL.setText("BA方位角");
                    btLeft.setText("右");
                } else {
                    leftRight = 1;
                    tvP1CAL.setText("CA方位角");
                    btLeft.setText("左");
                }
            }
        });
        mTvResult = (TextView)findViewById(R.id.tvResultP1);
    }

    private boolean checkInput() {
        for (int i=0; i<9; i++) {
            if (ets[i].getText().length() == 0) {
                return false;
            }
        }
        return true;
    }
    public void reset() {
        new AlertDialog.Builder(this)
                .setMessage("請按『確認』鈕清空輸入並重算\n或按『取消』返回")
                .setPositiveButton("確認",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset1();
                            }
                        }
                )
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }
                )
                .show();
    }
    private void reset1() {
        for (int i=0; i<12; i++) {
            ets[i].setText("");
        }
        ets[0].requestFocus();
    }
    private double inputDegree(String exp) {
        return Tools.auto2Deg(exp, degreeMode);
    }
    public void calc() {
        try {
            if (!checkInput()) {
                Toast.makeText(this, "請檢查是否每個欄位都有填值了", Toast.LENGTH_SHORT).show();
                return;
            }

            double x = Tools.parseDouble(ets[0]);
            double y = Tools.parseDouble(ets[1]);
            double h = Tools.parseDouble(ets[2]);
            double p = inputDegree(ets[3].getText().toString());
            double d = Tools.parseDouble(ets[4]);
            double b = inputDegree(ets[5].getText().toString());
            double c = inputDegree(ets[6].getText().toString());
            String ang = ets[7].getText().toString();
            if (ang.trim().equals("0")) ang = "1600";
            double q = inputDegree(ang);
            ang = ets[8].getText().toString();
            if (ang.trim().equals("0")) ang = "1600";
            double w = inputDegree(ang);

            double s = Tools.parseDouble(ets[9]);
            double z = Tools.parseDouble(ets[10]);
            double e = Tools.parseDouble(ets[11]);
            p = (p + 180.0)%360;
            double a = 180.0 - b - c;
            double r = d * Tools.SIN(c) / Tools.SIN(a);
            double l = d * Tools.SIN(b) / Tools.SIN(a);
            double m, n, u, v;
            if (leftRight == 1) {
                m = x + l * Tools.SIN(p);
                n = y + l * Tools.COS(p);
                u = x + r * Tools.SIN(p-a);
                v = y + r * Tools.COS(p-a);
            } else {
                m = x + r * Tools.SIN(p);
                n = y + r * Tools.COS(p);
                u = x + l * Tools.SIN(p+a);
                v = y + l * Tools.COS(p+a);
            }
            double[] res1 = Tools.len2Height(h, l, q, z, s, -1);
            double[] res2 = Tools.len2Height(h, r, w, e, s, -1);
            mTvResult.setText(String.format("基點C橫坐標 %.2f, 基點C縱坐標 %.2f\n"+
                    "另點B橫坐標 %.2f, 另點B縱坐標 %.2f\n"+
                    "基點C標高 %.2f, 另點B標高 %.2f\n", m, n, u, v, res1[0], res2[0]));
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, "計算發生錯誤", Toast.LENGTH_SHORT).show();
        }
    }
}
